numpy<2.0.0
scikit-learn>=1.5.1
scipy>=1.13.1
ase>=3.23.0
dscribe>=2.1.1
matplotlib>=3.9.1
