
# """Descriptor Embedding and Clustering for Atomisitic-environment Framework (DECAF)

# This is a sample code implementation of a article [1] with title:
# "A Fuzzy Classification Framework
# to Identify Equivalent Atoms in Complex Materials and Molecules"

# The file "./examples/sample_code.ipynb" contains demonstrations.
# Functions and methods given here.
# Please also refer the repository for updated details.

# Authors:
# King Chun Lai, Sebastian Matera, Christoph Scheurer, Karsten Reuter

# Affiliation:
# Fritz-Haber-Institut der Max-Planck-Gesellschaft,
# Faradayweg 4-6, 14195 Berlin, Germany

# Repository:
# https://gitlab.mpcdf.mpg.de/klai/decaf.git

# Corresponding Article:
# [1]:
# King Chun Lai, Sebastian Matera, Christoph Scheurer, Karsten Reuter: A Fuzzy Classification Framework to Identify Equivalent Atoms in Complex Materials and Molecules. Journal of Chemical Physics, 2023 (under review).

# Essential Dependence:
# Numpy, ASE, DScribe, Scikit Learn, Scipy

# License:
# This work is licensed under a Creative Commons Attribution 4.0 International License.
# http://creativecommons.org/licenses/by/4.0/

# ================================================================================
# """

# Packages
import os
import numpy as np
import copy
from ase import data as asedata
from ase import Atoms as aseatoms
from ase import build as asebuild
from ase.constraints import FixAtoms as asefixatoms
from dscribe.descriptors import SOAP
from scipy.spatial.distance import cdist, pdist, squareform
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.manifold import MDS
from sklearn.neighbors import KernelDensity



def get_SOAP(structure,NNDistance,species=[],SOAP_L_nmax=4,SOAP_L_lmax=3,SOAP_S_nmax=8,SOAP_S_lmax=4,SOAP_S_cut=(np.sqrt(1/2)+0.5),SOAP_S_sigma=(np.sqrt(1/2)+0.5)/8,SOAP_L_cut=(np.sqrt(3)/2+1),SOAP_L_sigma=(np.sqrt(3)/2+1)/8,periodic=True,atomID=np.array([])):
    """Get double-SOAP from ASE structures

    Parameters
    ----------
    structure : ASE Atoms object
        ASE atomic structure of class Atoms

    NNDistance : float
        Typical nearest neighbor distance in angstrom in the system, this is a length scale normalizing cut-off and sigma of SOAP.
            
    species : numpy.ndarray or None, optional
        List of atomic numbers to be visible in SOAP calculation, if None is provided, all chemical species are involved.
        Default: None

    SOAP_L_nmax : int, optional
        Maximum degree of radial basis function used in long range SOAP
        Default: 4
    
    SOAP_L_lmax : int, optional
        Maximum degree of spherical harmonics used in long range SOAP
        Default: 3
    
    SOAP_L_cut : float, optional
        Cut-off radius used in long range SOAP, in unit of NNDistance
        Default: (np.sqrt(3)/2+1)
    
    SOAP_L_sigma : float, optional
        Sigma of Gaussian used in atomic density of long range SOAP
        Default: (np.sqrt(3)/2+1)/8
    
    SOAP_S_nmax : int, optional
        Maximum degree of radial basis function used in short range SOAP
        Default: 8
    
    SOAP_S_lmax : int, optional
        Maximum degree of spherical harmonics used in short range SOAP
        Default: 4

    SOAP_S_cut : float, optional
        Cut-off radius used in short range SOAP, in unit of NNDistance
        Default: (np.sqrt(1/2)+0.5)

    SOAP_S_sigma : float, optional
        Sigma of Gaussian used in atomic density of short range SOAP
        Default: (np.sqrt(1/2)+0.5)/8
    
    periodic : bool, optional
        Switch of periodic boundary condition in SOAP expansion
        Default: True
    
    atomID : numpy.ndarray of shape (N), optional
        N Atom IDs in the structure where SOAP is evaluated. Default as an empty list, SOAP at all atoms in the structure are computed.
    
    Returns
    -------
    numpy.ndarray of shape (N,M)
        Concatenated SOAP in numpy array, N is the indices of SOAP computed atomID, M is the indices of SOAP_elements. For SOAP elements, long range SOAP comes first.

    """
#   Specify the involing chemical species in SOAP
    if len(species)== 0:
        SOAP_species=np.unique(structure.numbers).astype('int')
    else:
        SOAP_species=species
    # Initialize the SOAP calculator
    SoapDes_L= SOAP(species=SOAP_species, periodic=periodic,r_cut=NNDistance*SOAP_L_cut,sigma=NNDistance*SOAP_L_sigma,n_max=SOAP_L_nmax,l_max=SOAP_L_lmax,rbf="gto",average="off")
    SoapDes_S= SOAP(species=SOAP_species, periodic=periodic,r_cut=NNDistance*SOAP_S_cut,sigma=NNDistance*SOAP_S_sigma,n_max=SOAP_S_nmax,l_max=SOAP_S_lmax,rbf="gto",average="off")
    # Compute all SOAP of [structure1_C1,structure1_C2,structure1_O, structure2_C1, 2_C2, 2_O...]
    if atomID.shape[0]==0:
        SOAP_L=SoapDes_L.create(structure)
    else:
        SOAP_L=SoapDes_L.create(structure,centers=atomID)
    # Normalize the long-range SOAP
    SOAP_length_L=SOAP_L.shape[1]
    SOAP_L=SOAP_L/np.tile(np.linalg.norm(SOAP_L,axis=1),(SOAP_length_L,1)).T
    if atomID.shape[0]==0:
        SOAP_S=SoapDes_S.create(structure)
    else:
        SOAP_S=SoapDes_S.create(structure,centers=atomID)
    # Normalize the short-range SOAP
    SOAP_length_S=SOAP_S.shape[1]
    SOAP_S=SOAP_S/np.tile(np.linalg.norm(SOAP_S,axis=1),(SOAP_length_S,1)).T
    return np.hstack((SOAP_L,SOAP_S))

class embed_cluster():
    """
    A class for performing dimensionality reduction and clustering on a dataset.

    This class supports several dimensionality reduction methods (MDS, PCA, kPCA) and clustering algorithms 
    (mean-shift clustering, HDBSCAN, RobustSingleLinkage). It also handles large datasets by selecting 
    a subset of the data for training based on the specified `max_training_size`.

    Parameters
    ----------
    f_data : numpy.ndarray
        Full set of training data for initial dimension reduction, of shape `(samples, features)`.
    embed_str : str, optional
        Methodology of dimension reduction. Options are:

        - 'MDS' : Multidimensional scaling.
        - 'PCA' : Principal component analysis.
        - 'kPCA' : Kernel principal component analysis (requires a kernel function).

        Default is 'MDS'.
    cluster_str : str, optional
        Methodology of clustering. Options are:

        - 'MSC' : Mean-shift clustering (default).
        - 'HDBSCAN' : HDBSCAN clustering.

        Default is 'MSC'.
    max_training_size : int, optional
        Maximum size of the training set. If the sample size of `f_data` is larger than this value, 
        randomly select `max_training_size` samples to form the training set (`.t_data`). 
        If `max_training_size=0`, the full dataset is used. Default is 10000.
    kernel : callable, optional
        A function that returns a kernel matrix when using `kPCA` for dimensionality reduction.
        The function signature should be `kernel(arrayA, arrayB)`, where `arrayA` and `arrayB` are 
        arrays of shape `(samples, features)`. The two arrays must have the same number of features 
        but can have different numbers of samples.
    text_out : bool, optional
        If `True`, enables text output during the dimensionality reduction and clustering procedures.
        Default is `False`.

    Attributes
    ----------
    f_data : numpy.ndarray
        Full dataset input, of shape `(samples, features)`.
        Each row corresponds to an atom with an associated feature vector.
    t_data : numpy.ndarray
        The training set of data, selected from `f_data`. Its size depends on `max_training_size`.
    t_ID : numpy.ndarray
        Indices of the training data in the full dataset `f_data`. 
        Satisfies `f_data[t_ID] == t_data`.
    f_embedded : numpy.ndarray
        Dimensionally reduced data (embedded) for the full dataset `f_data`.
        Its shape is `(samples, embed_dimensions)`.
    t_embedded : numpy.ndarray
        Dimensionally reduced data (embedded) for the training set `t_data`.
        Its shape is `(training_samples, embed_dimensions)`.
    groupnum : int
        Number of groups identified by the clustering algorithm.
    f_gID : list ofnumpy.ndarray
        The group membership of each sample in the full dataset `f_data`. Each entry in the list is 
        a list of atom IDs that belong to the corresponding group. For example, `f_gID[2] = [5, 7, 10]` 
        means that group 2 contains atom IDs 5, 7, and 10 from `f_embedded`. The last list 
        (`f_gID[-1]` or `f_gID[groupnum]`) contains the atom IDs that do not belong to any group.
    t_gID : list ofnumpy.ndarray
        The group membership of each sample in the training set `t_data`. Each entry in the list is 
        a list of atom IDs that belong to the corresponding group in `t_embedded`. For example, 
        `t_gID[2] = [5, 7, 10]` means that group 2 contains atom IDs 5, 7, and 10 from `t_embedded`. 
        The last list (`t_gID[-1]` or `t_gID[groupnum]`) contains the atom IDs that do not belong to any group.

    """
    def __init__(self, f_data, embed_str='MDS', cluster_str='MSC', max_training_size=0, text_out=False, **kwargs):
        self.text_out=text_out
        self.embed_str=embed_str
        self.cluster_str=cluster_str

        # Selcting method for embedding
        if self.embed_str=='PCA':
            self.get_embed=self.get_PCA
            self.embed=self.embed_PCA
            self.truncate=self.truncate_PCA
        elif self.embed_str=='kPCA':
            self.kernel=kwargs.get('kernel',self.kernel_dot)
            self.get_embed=self.get_kPCA
            self.embed=self.embed_kPCA
            self.truncate=self.truncate_kPCA
        elif self.embed_str=='SketchMap':
            self.get_embed=self.get_SketchMap
        else:
            self.get_embed=self.get_cMDS
            self.embed=self.embed_cMDS
            self.truncate=self.truncate_cMDS

        # Selecting method for clustering
        if self.cluster_str=='HDBSCAN':
            self.get_cluster=self.get_HDBSCAN
            import hdbscan
            self.HDBSCAN_classify_fork=hdbscan.prediction.approximate_predict
            self.classify=self.HDBSCAN_classify
        else:
            self.get_cluster=self.get_MeanShift
            self.classify=self.MSC_classify
            self.bandwidth=0

        self.f_data=f_data
        if (self.f_data.shape[0]>max_training_size) and (max_training_size!=0):
            if self.text_out:
                print(f'Oversize of training set, include only {max_training_size} for training.\nPlease check class.t_ID for the ID of training set')
            self.t_ID = np.random.choice(np.arange(self.f_data.shape[0]), size=max_training_size, replace=False)
            self.t_ID = self.t_ID[self.t_ID.argsort()]
            self.t_data= self.f_data[self.t_ID]
        else:
            self.t_ID = np.arange(self.f_data.shape[0])
            self.t_data= self.f_data

    def workflow(self,**kwargs):
        """
        A workflow that wraps and passes arguments to embedding and clustering methods.

        This method acts as a wrapper for various dimensionality reduction and clustering methods, 
        such as `get_cMDS()`, `embed_cMDS()`, `get_MeanShift()`, and `MSC_classify()`. It accepts
        a flexible set of keyword arguments (`kwargs`) and passes them to the appropriate methods.

        Parameters
        ----------
        **kwargs : -
            Arbitrary keyword arguments. These are passed directly to the underlying embedding 
            and clustering methods (e.g., `get_embed()`, `get_cluster()`, `bandwidth_estimate()`).

        Attributes Affected
        ---------------------
        embed_op
            This function updates the attributes of     
        
        Returns
        -------
        None

        """

        if self.embed_str=='PCA':
            dim=kwargs.get('dim',2)
            less_stick_seg=kwargs.get('less_stick_seg',False)
            self.get_embed(dim=dim,less_stick_seg=less_stick_seg)
        elif self.embed_str=='kPCA':
            dim=kwargs.get('dim',2)
            less_stick_seg=kwargs.get('less_stick_seg',False)
            self.get_embed(dim=dim,less_stick_seg=less_stick_seg)
        elif self.embed_str=='SketchMap':
            dim=kwargs.get('dim',2)
            HD_sigma=kwargs.get('HD_sigma',0.028)
            LD_sigma=kwargs.get('LD_sigma',0)
            maxiter=kwargs.get('maxiter',10)
            prefix=kwargs.get('prefix','sketchmap')
            sm_path=kwargs.get('sm_path','./Compiled_SketchMap/')
            temp_path=kwargs.get('temp_path','./sm_temp/')
            self.get_embed(dim=dim,HD_sigma=HD_sigma, LD_sigma=LD_sigma, prefix=prefix, sm_path=sm_path, temp_path=temp_path)
        else:
            dim=kwargs.get('dim',2)
            less_stick_seg=kwargs.get('less_stick_seg',False)
            self.get_embed(dim=dim,less_stick_seg=less_stick_seg)

        if self.cluster_str=='HDBSCAN':
            min_cluster_size=kwargs.get('min_cluster_size',2)
            min_samples=kwargs.get('min_samples',1)
            reorder_mode_x=kwargs.get('reorder_mode_x',False)
            label_all=kwargs.get('label_all','LabelDistinct')
            # classify_mode=kwargs.get('classify_mode','ClusterCenter')
            self.get_cluster(min_cluster_size=min_cluster_size,min_samples=min_samples, label_all=label_all)#, classify_mode=classify_mode)
        else:
            bandwidth=kwargs.get('bandwidth',0)
            reorder_mode_x=kwargs.get('reorder_mode_x',False)
            accelerate=kwargs.get('accelerate',False)
            bandwidth_estimate =kwargs.get('bandwidth_estimate','Gaussian')
            if bandwidth_estimate=='HaarWavelet':
                sigma=kwargs.get('sigma',1.0)
                max_layer=kwargs.get('max_layer',10)
                min_layer=kwargs.get('min_layer',0)
                consecutive_fail=kwargs.get('consecutive_fail',1)
            else:
                sigma=kwargs.get('sigma',0.0092)
                max_layer=kwargs.get('max_layer',0)
                min_layer=kwargs.get('min_layer',0)
                consecutive_fail=kwargs.get('consecutive_fail',0)
            label_all=kwargs.get('label_all','LabelDistinct')
            classify_mode=kwargs.get('classify_mode','ClusterCenter')
            self.get_cluster(bandwidth=bandwidth,reorder_mode_x=reorder_mode_x, classify_mode=classify_mode, label_all=label_all, bandwidth_estimate =bandwidth_estimate,\
                            accelerate=accelerate,sigma=sigma,\
                            max_layer=max_layer,min_layer=min_layer,consecutive_fail=consecutive_fail)
        return None

    # """
    # ================================================================================
    # ========================Dimension Reduction Methodology=========================
    # ================================================================================
    # """
    
    def get_cMDS(self,dim=0,provided_metric=np.array([]),less_stick_seg=False):
        """
        Performs Classical Multidimensional Scaling (cMDS).

        This method reduces the dimensionality of a dissimilarity matrix using Classical MDS. 
        If the target dimension is not specified, the dimension is automatically determined 
        using the broken-stick model. The method can either compute the dissimilarity matrix 
        internally or use a provided one.

        Parameters
        ----------
        dim : int, optional
            The target number of dimensions for MDS, referred to as S' in the referenced article [1]_.
            If set to 0, the dimension will be automatically determined using the broken-stick model.
            Default is 0

        provided_metric : numpy.ndarray, optional
            A precomputed dissimilarity matrix for MDS. If not provided, the dissimilarity matrix 
            will be computed based on the input data.
            Default is np.array([])

        less_stick_seg : bool, optional
            Reduce the number of segments for the broken-stick model. Since PCA and MDS are equivalent
            with classical Euclidean distance as the dissimilarity measure, the maximum number of
            dimensions should not exceed `min(num_samples, num_features)`. If input True, this
            the number of segmanet will be set to `min(num_samples, num_features)`.
            Default is False
            
        Returns
        -------
        None

        Attributes
        ----------
        dim : int
            The embedding dimension is saved as attribute.
        embed_op : numpy.ndarray
            The operator for embedding is updated.
            This embedding operator for the MDS space, denoted as P in the referenced article [1]_.
        evals : numpy.ndarray
            The eigenvalues are saved as attribute.
            The eigenvalues in descending order, corresponding to the variance explained by each 
            dimension. Referred to as \lambda_a in the article [1]_.
        evec : numpy.ndarray
            The eigenvectors are saved as attribute.
            The eigenvectors associated with each eigenvalue, representing the principal directions 
            in the MDS space. Referred to as v_a in the article [1]_.
        f_embedded : numpy.ndarray
            The embedded full dataset is saved.
            Dimensionally reduced data (embedded) for the full dataset `f_data`.
            Its shape is `(samples, embed_dimensions)`.
        t_embedded : numpy.ndarray
            The embedded training dataset is saved.
            Dimensionally reduced data (embedded) for the training set `t_data`.
            Its shape is `(training_samples, embed_dimensions)`.
        

        Notes
        -----
        Classical MDS is a technique used for dimensionality reduction based on a dissimilarity 
        matrix. The method finds a configuration of points in a lower-dimensional space that 
        preserves the pairwise distances as much as possible.

        References
        ----------
        ..  [1] King Chun Lai, Sebastian Matera, Christoph Scheurer, Karsten Reuter: A Fuzzy Classification Framework to Identify Equivalent Atoms in Complex Materials and Molecules. J. Chem. Phys 159.2 (2023).
            DOI: https://doi.org/10.1063/5.0160369

        """
        check_norm= np.linalg.norm(self.t_data,axis=1)
        if (np.max(check_norm)-np.min(check_norm))>=0.001*np.mean(check_norm):
            print(f'The vector descriptor of the training data is not normalized!\n norm(max,min):{np.max(check_norm)},{np.min(check_norm)}')
            self.mds_normalized=False
        else:
            self.mds_normalized=True

        if provided_metric.shape[0]==0:
            # Shift the mean of input dataset to zero
            cen_t_data=self.t_data-np.tile(np.mean(self.t_data,axis=0),(self.t_data.shape[0],1))
            # Gram matrix
            gram=np.dot(cen_t_data,cen_t_data.T)
        else:
            inter_M=-0.5*np.square(provided_metric)
            inter_M1=np.mean(inter_M,axis=0)
            inter_M2=np.mean(inter_M1)
            inter_M1=np.tile(inter_M1,(inter_M.shape[0],1))
            gram=-(inter_M1+inter_M1.T-inter_M2-inter_M)

        # Eigen problem
        evals,F=np.linalg.eigh(gram)
        # Order the eigenvalues in descending order
        order=evals.argsort()[::-1]

        self.evals=evals[order]
        # Compute the embedding operator, and also embedded SOAP
        self.evec=F[:,order]
        self.truncate_cMDS(dim=dim,less_stick_seg=less_stick_seg)

        self.t_embedded=self.embed_cMDS(self.t_data)
        self.f_embedded=self.embed_cMDS(self.f_data)
        if self.text_out:
            print(f'MDS Dimension {self.dim}')
            print(f'Largest 5 eigenvalues: {self.evals[:5]}')
        return None

    def truncate_cMDS(self, dim=0 ,less_stick_seg=False):
        """
        Truncates the components in Classical Multidimensional Scaling (cMDS).

        This method reduces the dimensionality of the data in cMDS by truncating the components.
        If the target dimension (`dim`) is not specified, the dimension will be automatically
        determined using a broken-stick model. The maximum number of dimensions will not exceed
        the minimum of the number of samples and the number of features.

        Parameters
        ----------
        dim : int, optional
            The target number of dimensions for MDS, referred to as S' in the referenced article [1]_.
            If set to 0, the dimension will be automatically determined using a broken-stick model.
            Default is 0.

        less_stick_seg : bool, optional
            Reduce the number of segments for the broken-stick model. Since PCA and MDS are equivalent
            with classical Euclidean distance as the dissimilarity measure, the maximum number of
            dimensions should not exceed `min(num_samples, num_features)`. If input True, this
            the number of segmanet will be set to `min(num_samples, num_features)`.
            Default is False

        Attributes
        ----------
        embed_op : numpy.ndarray
            The operator for embedding is updated.
            This embedding operator for the MDS space, denoted as P in the referenced article [1]_.
        dim : int
            The embedding dimension is saved as attribute.
        
        Notes
        -----
        The broken-stick model is used to determine the appropriate number of dimensions for
        dimensionality reduction. In the context of cMDS, it is used to truncate the number of
        components based on the explained variance.

        """
        if dim==0:
            # Broken-stick approach to estimate the suitable MDS dimension
            if less_stick_seg:
                maxdim=np.min((self.t_data.shape[0],self.t_data.shape[1]))
            else:
                maxdim=self.evals.shape[0]
            self.get_broken_stick(maxdim)
            self.dim=np.sum(self.broken_stick[:maxdim]<= (self.evals[:maxdim]/np.sum(self.evals[:maxdim])))
        else:
            self.dim=dim
        self.embed_op=(self.evec[:,:self.dim]*np.tile(1/np.sqrt(self.evals[:self.dim]),(self.evec.shape[0],1))).T
        self.embed_op_trans= np.array([np.dot(item,item) for item in self.t_data])
        return None


    def embed_cMDS(self, n_data):
        """
        Embeds new environments using Classical Multidimensional Scaling (cMDS).

        This method takes new data that has not been previously embedded and performs 
        dimensionality reduction using cMDS. The new data should have the same format 
        as the original dataset used for training.

        Parameters
        ----------
        n_data : numpy.ndarray
            New data to be embedded. It should have the same format as `.f_data` and 
            be of shape `(num_samples, num_features)`, where `num_samples` is the 
            number of samples (atoms) and `num_features` is the dimensionality 
            of the original data.
            Indices are expected to be in the format `[atomID, dimension]`.

        Returns
        -------
        embedded : numpy.ndarray
            The newly embedded data in the reduced dimensionality, denoted as \chi' in 
            the referenced article [1]_. The shape of the output is `(num_samples, MDS_dimension)`, 
            where `MDS_dimension` is the target number of dimensions after applying cMDS.
            Indices are in the format `[atomID, MDS_dimension]`.

        Notes
        -----
        The embedding process projects the new data into the same MDS space that was 
        constructed using the original dataset. The new embedded data will have the 
        same reduced dimensionality as the original MDS embedding.

        """
        # Computet the matrix of squared pairwise distances (D^2) between FULL SOAP
        # in dataset and incoming structure(s)
        if self.mds_normalized:
            d2=np.square(cdist(self.t_data,n_data))
        else:
            d2=np.tile(self.embed_op_trans,(n_data.shape[0],1)).T-np.square(cdist(self.t_data,n_data))
        # Embed the incoming SOAP with the operator P.
        embedded=np.dot(self.embed_op,d2).T
        return embedded

    def get_PCA(self, dim=0, less_stick_seg=False):
        """
        Performs Classical Principal Component Analysis (PCA).

        This method reduces the dimensionality of the data using PCA. If the target dimension 
        (`dim`) is not specified, the dimensionality will be automatically determined using 
        a broken-stick model. The method also accepts a parameter to control the truncation 
        of components.

        Parameters
        ----------
        dim : int, optional
            The target number of dimensions for PCA, referred to as S' in the referenced 
            article [1]_. If set to 0, the number of dimensions will be automatically determined 
            using a broken-stick model. Default is 0.

        less_stick_seg : bool, optional
            Reduce the number of segments for the broken-stick model. Since PCA and MDS are equivalent
            with classical Euclidean distance as the dissimilarity measure, the maximum number of
            dimensions should not exceed `min(num_samples, num_features)`. If input True, this
            the number of segmanet will be set to `min(num_samples, num_features)`.
            Default is False

        Returns
        -------
        None

        Attributes
        ----------
        dim : int
            The embedding dimension is saved as attribute.
        embed_op : numpy.ndarray
            The operator for embedding is updated.
            This embedding operator for the PCA space, denoted as P in the referenced article [1]_.
        evals : numpy.ndarray
            The eigenvalues are saved as attribute.
            The eigenvalues in descending order, corresponding to the variance explained by each 
            dimension. Referred to as \lambda_a in the article [1]_.
        evec : numpy.ndarray
            The eigenvectors are saved as attribute.
            The eigenvectors associated with each eigenvalue, representing the principal directions 
            in the MDS space. Referred to as v_a in the article [1]_.
        f_embedded : numpy.ndarray
            The embedded full dataset is saved.
            Dimensionally reduced data (embedded) for the full dataset `f_data`.
            Its shape is `(samples, embed_dimensions)`.
        t_embedded : numpy.ndarray
            The embedded training dataset is saved.
            Dimensionally reduced data (embedded) for the training set `t_data`.
            Its shape is `(training_samples, embed_dimensions)`.

        Notes
        -----
        PCA is a widely used technique for dimensionality reduction, which projects data onto 
        orthogonal axes (principal components) that maximize the variance. The broken-stick 
        model can be used to determine the optimal number of components to retain.

        """
        data_cen= self.t_data-np.tile(np.mean(self.t_data,axis=0),(self.t_data.shape[0],1))
        cov=np.dot(data_cen.T,data_cen)/self.t_data.shape[0]
        self.cov=cov
        v,F=np.linalg.eigh(cov)
        order=v.argsort()[::-1]
        self.evals=v[order]
        self.evec=F[:,order]
        self.truncate_PCA(dim=dim, less_stick_seg=less_stick_seg)
        self.t_embedded=self.embed_PCA(self.t_data)
        self.f_embedded=self.embed_PCA(self.f_data)
        if self.text_out:
            print(f'PCA Dimension {self.dim}')
            print(f'Largest 5 eigenvalues: {self.evals[:5]}')
        return None

    def truncate_PCA(self, dim=0,less_stick_seg=False):
        """
        Truncates components in Classical Multidimensional Scaling (cMDS) or PCA.

        This method reduces the dimensionality of the data by truncating the components 
        based on specified criteria. If the target dimension (`dim`) is not specified, 
        the dimension will be automatically determined using a broken-stick model. 
        The method can also utilize the `less_stick_seg` parameter to control the number 
        of segments in the broken-stick model.

        Parameters
        ----------
        dim : int, optional
            The target number of dimensions for MDS, referred to as S' in the referenced 
            article [1]_. If set to 0, the number of dimensions will be automatically determined 
            using a broken-stick model. Default is 0.

        less_stick_seg : bool, optional
            Reduce the number of segments for the broken-stick model. Since PCA and MDS are equivalent
            with classical Euclidean distance as the dissimilarity measure, the maximum number of
            dimensions should not exceed `min(num_samples, num_features)`. If input True, this
            the number of segmanet will be set to `min(num_samples, num_features)`.
            Default is False

        Returns
        -------
        None

        Attributes
        ----------
        embed_op : numpy.ndarray
            The operator for embedding is updated.
            This embedding operator for the PCA space, denoted as P in the referenced article [1]_.
        dim : int
            The embedding dimension is saved as attribute.

        Notes
        -----
        PCA is a widely used technique for dimensionality reduction, which projects data onto 
        orthogonal axes (principal components) that maximize the variance. The broken-stick 
        model can be used to determine the optimal number of components to retain.

        """
        if dim==0:
            # Broken-stick approach to estimate the suitable MDS dimension
            if less_stick_seg:
                maxdim=np.min((self.t_data.shape[0],self.t_data.shape[1]))
            else:
                maxdim=self.evals.shape[0]
            self.get_broken_stick(maxdim)
            self.dim=np.sum(self.broken_stick[:maxdim]<= (self.evals[:maxdim]/np.sum(self.evals[:maxdim])))
        else:
            self.dim=dim
        self.embed_op= self.evec[:,:self.dim]
        return None

    def embed_PCA(self, n_data):
        """
        Embeds new environments using Principal Component Analysis (PCA).

        This method takes new data that has not been previously embedded and performs 
        dimensionality reduction using the PCA space defined by the original training data. 
        The new data should have the same format as the original dataset.

        Parameters
        ----------
        n_data : numpy.ndarray
            New data to be embedded. It should have the same format as `.f_data` and 
            be of shape `(num_samples, num_features)`, where `num_samples` is the number 
            of samples (atoms) and `num_features` is the dimensionality of the original data.
            Indices are expected to be in the format `[atomID, dimension]`.

        Returns
        -------
        numpy.ndarray
            The newly embedded data in the reduced dimensionality, denoted as \chi' in 
            the referenced article [1]_. The shape of the output is `(num_samples, PCA_dimension)`, 
            where `PCA_dimension` is the target number of dimensions after applying PCA.
            Indices are in the format `[atomID, PCA_dimension]`.

        Notes
        -----
        The embedding process projects the new data into the same PCA space that was 
        constructed using the original dataset. The new embedded data will have the 
        same reduced dimensionality as the original PCA embedding.

        """
        return np.dot(n_data,self.embed_op)

    def get_kPCA(self,dim=0,less_stick_seg=False):
        """
        Performs Classical Kernel Principal Component Analysis (kPCA).

        This method reduces the dimensionality of the data using kPCA. If the target dimension 
        (`dim`) is not specified, the dimensionality will be automatically determined using 
        a broken-stick model. The method also accepts a parameter to control the truncation 
        of components.

        Parameters
        ----------
        dim : int, optional
            The target number of dimensions for kPCA, referred to as S' in the referenced 
            article [1]. If set to 0, the number of dimensions will be automatically determined 
            using a broken-stick model.
            Default is 0.

        less_stick_seg : bool, optional
            Reduce the number of segments for the broken-stick model. Since PCA and MDS are equivalent
            with classical Euclidean distance as the dissimilarity measure, the maximum number of
            dimensions should not exceed `min(num_samples, num_features)`. If input True, this
            the number of segmanet will be set to `min(num_samples, num_features)`.
            Default is False

        Returns
        -------
        None

        Attributes
        ----------
        dim : int
            The embedding dimension is saved as attribute.
        embed_op : numpy.ndarray
            The operator for embedding is updated.
            This embedding operator for the kPCA space, denoted as P in the referenced article [1]_.
        evals : numpy.ndarray
            The eigenvalues are saved as attribute.
            The eigenvalues in descending order, corresponding to the variance explained by each 
            dimension. Referred to as \lambda_a in the article [1]_.
        evec : numpy.ndarray
            The eigenvectors are saved as attribute.
            The eigenvectors associated with each eigenvalue, representing the principal directions 
            in the MDS space. Referred to as v_a in the article [1]_.
        f_embedded : numpy.ndarray
            The embedded full dataset is saved.
            Dimensionally reduced data (embedded) for the full dataset `f_data`.
            Its shape is `(samples, embed_dimensions)`.
        t_embedded : numpy.ndarray
            The embedded training dataset is saved.
            Dimensionally reduced data (embedded) for the training set `t_data`.
            Its shape is `(training_samples, embed_dimensions)`.

        Notes
        -----
        Kernel PCA (kPCA) is an extension of traditional PCA that allows for nonlinear 
        dimensionality reduction by applying PCA in a transformed feature space defined 
        by a kernel function. The broken-stick model can be used to determine the optimal 
        number of components to retain.

        """
        kernelM= self.kernel(self.t_data,self.t_data)
        allone=np.ones_like(kernelM)/kernelM.shape[0]
        kernelM_cen= kernelM-np.dot(allone,kernelM)-np.dot(kernelM,allone)+np.linalg.multi_dot([allone,kernelM,allone])
        v,F=np.linalg.eigh(kernelM_cen)
        order=np.abs(v).argsort()[::-1]
        if v[order[0]]<0:
            v=-v
        self.evals=v[order]
        self.evec=F[:,order]
        self.truncate_kPCA(dim=dim, less_stick_seg=less_stick_seg)
        self.t_embedded=self.embed_kPCA(self.t_data)
        self.f_embedded=self.embed_kPCA(self.f_data)
        if self.text_out:
            print(f'kPCA Dimension {self.dim}')
            print(f'Largest 5 eigenvalues: {self.evals[:5]}')
        return None

    def truncate_kPCA(self, dim=0,less_stick_seg=False):
        """
        Truncates components in Kernel Principal Component Analysis (kPCA).

        This method reduces the dimensionality of the data by truncating components in kPCA.
        If the target dimension (`dim`) is not specified, the dimensionality will be determined
        using a broken-stick model. The method can also use the `less_stick_seg` parameter 
        to control the number of segments in the broken-stick model.

        Parameters
        ----------
        dim : int, optional
            The target number of dimensions for kPCA, referred to as S' in the referenced 
            article [1]_. If set to 0, the number of dimensions will be automatically determined 
            using a broken-stick model.
            Default is 0.

        less_stick_seg : bool, optional
            Reduce the number of segments for the broken-stick model. Since PCA and MDS are equivalent
            with classical Euclidean distance as the dissimilarity measure, the maximum number of
            dimensions should not exceed `min(num_samples, num_features)`. If input True, this
            the number of segmanet will be set to `min(num_samples, num_features)`.
            Default is False

        Returns
        -------
        None

        Attributes
        ----------
        embed_op : numpy.ndarray
            The operator for embedding is updated.
            This embedding operator for the PCA space, denoted as P in the referenced article [1]_.
        dim : int
            The embedding dimension is saved as attribute.

        Notes
        -----
        The broken-stick model is used to determine the optimal number of dimensions for 
        dimensionality reduction. The maximum number of dimensions should not exceed 
        `min(num_samples, num_features)`, which is derived from the equivalence between 
        PCA and MDS with classical Euclidean distance as the dissimilarity measure.

        """
        if dim==0:
            # Broken-stick approach to estimate the suitable MDS dimension
            if less_stick_seg:
                maxdim=np.min((self.t_data.shape[0],self.t_data.shape[1]))
            else:
                maxdim=self.evals.shape[0]
            self.get_broken_stick(maxdim)
            self.dim=np.sum(self.broken_stick[:maxdim]<= (self.evals[:maxdim]/np.sum(self.evals[:maxdim])))
        else:
            self.dim=dim
        self.embed_op=self.evec[:,:self.dim]/np.tile(np.sqrt(self.evals[:self.dim]),(self.evec.shape[0],1))
        return None

    def embed_kPCA(self, n_data):
        """
        Embeds new environments using Kernel Principal Component Analysis (kPCA).

        This method takes new data that has not been previously embedded and performs 
        dimensionality reduction using the kPCA space defined by the original training data.
        The new data should have the same format as the original dataset.

        Parameters
        ----------
        n_data : numpy.ndarray
            New data to be embedded. It should have the same format as `.f_data` and 
            be of shape `(num_samples, num_features)`, where `num_samples` is the number 
            of samples (atoms) and `num_features` is the dimensionality of the original data.
            Indices are expected to be in the format `[atomID, dimension]`.

        Returns
        -------
        numpy.ndarray
            The newly embedded data in the reduced dimensionality, denoted as \chi' in 
            the referenced article [1]_. The shape of the output is `(num_samples, kPCA_dimension)`, 
            where `kPCA_dimension` is the target number of dimensions after applying kPCA.
            Indices are in the format `[atomID, kPCA_dimension]`.

        Notes
        -----
        The embedding process projects the new data into the same kPCA space that was 
        constructed using the original dataset. The new embedded data will have the 
        same reduced dimensionality as the original kPCA embedding.

        """
        return np.dot(self.kernel(n_data,self.t_data),self.embed_op)

    def get_SketchMap(self, dim=2, HD_sigma=0.137, LD_sigma=0, HD_A=0, HD_B=1, LD_a=0, LD_b=1, prefix='sketchmap', sm_path='./Compiled_SketchMap/', temp_path='sm_temp/', maxiter=10):
        """
        Performs Sketch Map embedding for dimensionality reduction.

        This method provides an interface for performing Sketch Map embedding using the 
        original compiled package. The method is adapted from the "sketch-map.sh" script 
        provided in the original package, with some functionalities truncated (e.g., directly 
        inputting the similarity matrix). For more information about Sketch Map, please refer 
        to the original package and publication [2]_.

        Parameters
        ----------
        sm_path : str, optional
            Path to the compiled Sketch Map executable (dimred). Default is './Compiled_SketchMap/'.
        temp_path : str, optional
            Path for storing output files generated by Sketch Map. Default is 'sm_temp/'.
        prefix : str, optional
            Prefix for the file names of the Sketch Map outputs. Default is 'sketchmap'.
        maxiter : int, optional
            Maximum number of iterative steps for the embedding process. Default is 10.
        dim : int, optional
            Dimension of the embedding. Default is 2.
        HD_sigma : float, optional
            Hyperparameter for the high-dimensional sigmoid function. Default is 0.137.
        HD_A : float, optional
            Parameter for high-dimensional sigmoid function, with default 0. It will be 
            automatically adjusted based on the high-dimensional data if set to 0.
        HD_B : float, optional
            Parameter for high-dimensional sigmoid function. Default is 1.
        LD_sigma : float, optional
            Hyperparameter for the low-dimensional sigmoid function. If set to 0, it will 
            be adjusted based on `HD_sigma`. Default is 0.
        LD_a : float, optional
            Parameter for low-dimensional sigmoid function, with default 0. It will be 
            automatically adjusted based on the embedding dimension (`dim`) if set to 0.
        LD_b : float, optional
            Parameter for low-dimensional sigmoid function. Default is 1.

        Returns
        -------
        None

        Attributes
        ----------
        dim : int
            The embedding dimension is saved as attribute.
        f_embedded : numpy.ndarray
            The embedded full dataset is saved.
            Dimensionally reduced data (embedded) for the full dataset `f_data`.
            Its shape is `(samples, embed_dimensions)`.
        t_embedded : numpy.ndarray
            The embedded training dataset is saved.
            Dimensionally reduced data (embedded) for the training set `t_data`.
            Its shape is `(training_samples, embed_dimensions)`.
        sm_path : str
            Path to the compiled Sketch Map executable.
        temp_path : str
            Path to the output files generated by Sketch Map.
        name_t_in : str
            Path to the data file generated from `.t_data` for input to Sketch Map.
        name_t_out : str
            Path to the output data file after iterations.
        grid : any
            Grid parameter used at the end of the iteration.
        fun_HD : str
            String representing the high-dimensional sigmoid function parameters (`"\sigma_HD,A,B"`).
        fun_LD : str
            String representing the low-dimensional sigmoid function parameters (`"\sigma_LD,A,B"`).

        Notes
        -----
        This function acts as a Python translation of the "sketch-map.sh" script provided 
        in the original Sketch Map package, with some functionalities omitted (such as direct 
        input of a similarity matrix). For details on how to compile and use Sketch Map, 
        refer to the original documentation and source [2]_.

        References
        ----------
        ..  [2] M. Ceriotti, G. A. Tribello, and M. Parrinello, "Simplifying the representation of complex free-energy landscapes using sketch-map", Proc. Natl. Acad. Sci. U.S.A., 108, 13023–13028 (2011). 
            URL : https://github.com/lab-cosmo/sketchmap
        
        """
        import subprocess
        if temp_path[-1]!='/':
            try:
                os.mkdir(temp_path)
            except:
                pass
            temp_path+='/'
        else:
            try:
                os.mkdir(temp_path[:-1])
            except:
                pass
        if sm_path[-1]!='/':
            sm_path+='/'
        name_in= temp_path+prefix+'_in.dat'
        name_out= temp_path+prefix+'_out.imds'
        name_err= temp_path+'log'
        name_dimred=sm_path+'dimred'
        name_init=sm_path+'tmp'

        allfiles=os.listdir('./')
        file_global=[item for item in allfiles if 'global.'==item[:7]]
        for item in file_global:
            os.remove('./'+item)
        allfiles=os.listdir(temp_path)
        file_old=[item for item in allfiles if (prefix+'_out') in item]
        for item in file_old:
            os.remove(temp_path+item)

        with open(name_in,'w') as obj_stdin:
            temp_text='\n'.join([' '.join([str(item2) for item2 in item]) for item in self.t_data])+'\n'
            obj_stdin.write(temp_text)
            del temp_text

        HD=self.t_data.shape[1]
        preopt=100
        if LD_sigma==0:
            LD_sigma=HD_sigma
        if HD_A==0:
            HD_A=HD
        if LD_a==0:
            LD_a=dim

        fun_HD=','.join([str(HD_sigma),str(HD_A),str(HD_B)])
        fun_LD=','.join([str(LD_sigma),str(LD_a),str(LD_b)])

        imix=1.0
        gopt=3
        if self.text_out:
            print(f'Sketch Map Iteration Initiated {HD}D to {dim}D')
        cmd_default= [name_dimred, '-vv', '-D', str(HD), '-d', str(dim), '-center', '-preopt', str(preopt)]
        with open(name_in, 'r') as obj_stdin, open(name_out, 'w') as obj_stdout, open(name_err, 'w') as obj_stderr:
            subprocess.call(cmd_default,stdin=obj_stdin, stdout=obj_stdout, stderr=obj_stderr, shell=False)
        with open(name_out, 'r') as last_map:
            lines=last_map.readlines()
            NERR=lines[3].split()
            NERR=float(NERR[-1])
            lines=[item.split()[:dim] for item in lines[5:]]
            grid=np.max(np.linalg.norm(np.array([[float(item2) for item2 in item] for item in lines]), axis=1))*1.2
            del lines
        if self.text_out:
            print(f'Initial MDS, Residual Error: {NERR}, Grid: {grid}')

        with open(name_init, 'w') as obj_tmp, open(name_out, 'r') as obj_stdout:
            lines=obj_stdout.readlines()[5:]
            lines='\n'.join([' '.join(item.split()[:dim]) for item in lines])+'\n'
            obj_tmp.write(lines)
            del lines

        name_out= temp_path+prefix+'_out.ismap'
        cmd= cmd_default+['-fun-hd', fun_HD, '-fun-ld', fun_LD, '-init', name_init]
        with open(name_in, 'r') as obj_stdin, open(name_out, 'w') as obj_stdout, open(name_err, 'w') as obj_stderr:
                subprocess.call(cmd,stdin=obj_stdin, stdout=obj_stdout, stderr=obj_stderr, shell=False)
        with open(name_out, 'r') as last_map:
            lines=last_map.readlines()
            SMERR=lines[3].split()
            SMERR=float(SMERR[-1])
            del lines
        if self.text_out:
            print(f'Initial SMAP, S.Error: {SMERR}, HD params(\sigma,A,B) '+ fun_HD+ ', LD params(\sigma,a,b) '+fun_LD)
        preopt=50
        cmd_default= [name_dimred, '-vv', '-D', str(HD), '-d', str(dim), '-center', '-preopt', str(preopt)]
        for iteration in np.arange(1,1+maxiter):
            MDERR=NERR
            name_out= temp_path+prefix+'_out.gmds_'+str(iteration)
            cmd= cmd_default+['-grid', str(grid)+',21,201','-fun-hd', fun_HD, '-fun-ld', fun_LD, '-init', name_init, '-gopt', str(gopt), '-imix', str(imix)]
            with open(name_in, 'r') as obj_stdin, open(name_out, 'w') as obj_stdout, open(name_err, 'w') as obj_stderr:
                subprocess.call(cmd,stdin=obj_stdin, stdout=obj_stdout, stderr=obj_stderr, shell=False)
            with open(name_init, 'w') as obj_tmp, open(name_out, 'r') as obj_stdout:
                lines=obj_stdout.readlines()[5:]
                lines='\n'.join([' '.join(item.split()[:dim]) for item in lines])+'\n'
                obj_tmp.write(lines)
                del lines

            with open(name_out, 'r') as last_map:
                lines=last_map.readlines()
                NERR=lines[3].split()
                NERR=float(NERR[-1])
                lines=[item.split()[:dim] for item in lines[5:]]
                grid=np.max(np.linalg.norm(np.array([[float(item2) for item2 in item] for item in lines]), axis=1))*1.2
                del lines
                if self.text_out:
                    print(f'Iteration: {iteration}, Residual Error: {NERR}, Grid: {grid}, imix: {imix}')
            new=SMERR/(SMERR+NERR)
            if new<0.1:
                new=0.1
            elif new>0.5:
                new=0.5
            imix*=new

            if ((MDERR-NERR)/NERR)**2<0.0001 and iteration>1:
                if self.text_out:
                    print(f'SketchMap iteration done, criteria:{((MDERR-NERR)/NERR)**2}<10e-4')
                break
        name_out= temp_path+prefix+'_out.gmds'
        gopt=10
        cmd= cmd_default+['-grid', str(grid)+',21,201','-fun-hd', fun_HD, '-fun-ld', fun_LD, '-init', name_init, '-gopt', str(gopt), '-imix', str(imix)]
        with open(name_in, 'r') as obj_stdin, open(name_out, 'w') as obj_stdout, open(name_err, 'w') as obj_stderr:
                subprocess.call(cmd,stdin=obj_stdin, stdout=obj_stdout, stderr=obj_stderr, shell=False)
        if self.text_out:
            print('Done Final EMbedding')
        allfiles=os.listdir('./')
        file_global=[item for item in allfiles if 'global.'==item[:7]]
        for item in file_global:
            os.remove('./'+item)
        os.remove(name_init)

        with open(name_out,'r') as obj_stdout:
            lines=obj_stdout.readlines()[5:]
            lines=[item.split()[:dim] for item in lines]
            self.t_embedded=np.array([[float(item2) for item2 in item] for item in lines])
            del lines
        self.prefix=prefix
        self.name_t_in=name_in
        self.name_t_out=temp_path+prefix+'_out.dat'
        with open(self.name_t_out, 'w') as obj_t_out, open(name_out, 'r') as obj_stdout:
            lines=obj_stdout.readlines()[5:]
            lines='\n'.join([' '.join(item.split()[:dim]) for item in lines])+'\n'
            obj_t_out.write(lines)
            del lines
        self.sm_path= sm_path
        self.temp_path= temp_path
        self.grid= grid
        self.fun_HD=fun_HD
        self.fun_LD=fun_LD

        # The wrapping of 'dimproj' here is questionable about consistency between
        # the mappings of \xhi (high-D) to \chi' (low-D).
        # The mapping from 'dimred' and 'dimproj' are not consistent
        # We thus remove in general any active use of the embedding function
        self.f_embedded=self.t_embedded
        return None

    def embed_SketchMap(self, n_data):
        """
        Embeds new environments using Sketch Map [2]_.

        This method takes new data that has not been previously embedded and performs 
        dimensionality reduction using the Sketch Map space defined by the original 
        training data. The new data should have the same format as the original dataset.

        WARNING: This wrapping interface is not well tested, and users should use it 
        with caution.

        Parameters
        ----------
        n_data : numpy.ndarray
            New data to be embedded. It should have the same format as `.f_data` and 
            be of shape `(num_samples, num_features)`, where `num_samples` is the number 
            of samples (atoms) and `num_features` is the dimensionality of the original data.
            Indices are expected to be in the format `[atomID, dimension]`.

        Returns
        -------
        n_embedded : numpy.ndarray
            The newly embedded data in the reduced dimensionality, denoted as \chi' in 
            the referenced article [2]_. The shape of the output is `(num_samples, sketch_map_dimension)`, 
            where `sketch_map_dimension` is the target number of dimensions after applying Sketch Map.
            Indices are in the format `[atomID, sketch_map_dimension]`.

        """
        print('WARNING: This wrapping interface is not well tested, user might use with caution')
        import subprocess
        name_new_in= self.temp_path+self.prefix+'_in_new.dat'
        name_new_out= self.temp_path+self.prefix+'_out_new.gmds'
        name_err= self.temp_path+'log'
        name_dimproj=self.sm_path+'dimproj'
        name_init=self.sm_path+'tmp'
        HD = n_data.shape[1]
        LD = self.t_embedded.shape[1]

        with open(name_new_in,'w') as obj_stdin:
            temp_text='\n'.join([' '.join([str(item2) for item2 in item]) for item in n_data])+'\n'
            obj_stdin.write(temp_text)
            del temp_text

        cgmin=3
        cmd= [name_dimproj, '-vv', '-D', str(HD), '-d', str(LD),\
                        '-P', self.name_t_in, '-p', self.name_t_out,\
                        'grid', str(self.grid)+',21,201',\
                        '-fun_hd', self.fun_HD,'-fun_ld', self.fun_LD, '-print'\
                        '-cgmin', str(cgmin)]
        with open(name_new_in, 'r') as obj_stdin, open(name_new_out, 'w') as obj_stdout, open(name_err, 'w') as obj_stderr:
                subprocess.call(cmd,stdin=obj_stdin, stdout=obj_stdout, stderr=obj_stderr, shell=False)

        with open(name_new_out,'r') as obj_stdout:
            lines=obj_stdout.readlines()
            lines=[item.split()[:LD] for item in lines]
            n_embedded=np.array([[float(item2) for item2 in item] for item in lines])
            del lines
        return n_embedded

    def get_MeanShift(self,bandwidth=0,accelerate=False,reorder_mode_x=False,sigma=0,label_all='LabelAll',classify_mode='ClusterCenter',bandwidth_estimate='Gaussian',max_layer=10,min_layer=5,consecutive_fail=2):
        """
        Performs Mean Shift Clustering.

        This method applies Mean Shift Clustering (MSC) to the data using specified 
        parameters for bandwidth estimation, clustering acceleration, and labeling. 
        The bandwidth parameter controls the clustering resolution, and other parameters 
        help adjust the clustering methodology and behavior.

        Parameters
        ----------
        bandwidth : float, optional
            The bandwidth for Mean Shift Clustering, referred to as \delta_MSC in the article [1]_. 
            Default is 0.
        accelerate : bool, optional
            Whether to use acceleration in the bandwidth estimation via `get_bandwidth_Gaussian()` or  `get_bandwidth_HaarWavelet()`. 
            Default is `False`.
        reorder_mode_x : bool, optional
            Whether to reorder the group ID using `reorder_groupID()`. Default is `False`.
        sigma : float, optional
            Input parameter for `get_bandwidth_Gaussian()` or  `get_bandwidth_HaarWavelet()`.
            Default is 0, which implies using the default value 0.0092 for Gaussian; or 1.0 for HaarWavelet
        label_all : str, optional
            Specifies the labeling method for data points:
            - `'LabelDistinct'`: Assigns a data point as distinct if it has no neighbor 
            within the radius of `bandwidth` from `.t_embedded`. This is the default mode.
            - `'LabelAll'`: Labels data points to the closest cluster center.
            - `'Both'`: Outputs both distinct and all labeled clusters as a tuple.
            Default is `'LabelAll'`.
        classify_mode : str, optional
            The mode for classifying data points. Default is `'ClusterCenter'`.
        bandwidth_estimate : str, optional
            Method for estimating the bandwidth, with `'Gaussian'` as the default choice.
        max_layer : int, optional
            The maximum number of clustering layers. Default is 10.
        min_layer : int, optional
            The minimum number of clustering layers. Default is 5.
        consecutive_fail : int, optional
            The number of consecutive failures allowed during clustering. Default is 2.

        Returns
        -------
        None

        Attributes
        ----------
        bandwidth : float
            The clustering bandiwdth is saved as attribute.
        groupnum : int
            The number of groups out of the clustering, is saved as attribute.
        cluster_centers :numpy.ndarray
            List of cluster centers in embedded space is saved as attribute.
        f_gID : list ofnumpy.ndarray
            The group membership of each sample in the full dataset `f_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group. For example, `f_gID[2] = [5, 7, 10]` 
            means that group 2 contains atom IDs 5, 7, and 10 from `f_embedded`. The last list 
            (`f_gID[-1]` or `f_gID[groupnum]`) contains the atom IDs that do not belong to any group.
        t_gID : list ofnumpy.ndarray
            The group membership of each sample in the training set `t_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group in `t_embedded`. For example, 
            `t_gID[2] = [5, 7, 10]` means that group 2 contains atom IDs 5, 7, and 10 from `t_embedded`. 
            The last list (`t_gID[-1]` or `t_gID[groupnum]`) contains the atom IDs that do not belong to any group.
        classify_mode : str
            The classify mode is saved as attribute.
        reorder_mode_x : bool
            reorder_mode_x is saved as attribute.
        sigma : float
            sigma for bandwidth estimation is saved as attribute
        label_all : str
            label_all is saved as attribute
        hist_distribution :numpy.ndarray
            If bandwidth was set true in the argument of get_MeanShift(), the result of bandwidth estimation is saved in details. This is the density of each bin of the distribution of pairwise distances in the embedded descriptor space.
        hist_bins :numpy.ndarray
            If bandwidth was set true in the argument of get_MeanShift(), the result of bandwidth estimation is saved in details. This is the location of each bin of the distribution of pairwise distances in the embedded descriptor space.
        hist_binwidth :numpy.ndarray
            If bandwidth was set true in the argument of get_MeanShift(), the result of bandwidth estimation is saved in details. This is the width of each bin of the distribution of pairwise distances in the embedded descriptor space.
        bandwidth_full :numpy.ndarray
            If bandwidth was set true in the argument of get_MeanShift(), the full list of bandwidth candidates is saved.

        Notes
        -----
        Mean Shift Clustering is a non-parametric clustering technique that estimates 
        cluster centers by shifting data points towards areas of higher density in the 
        feature space. The bandwidth parameter controls the clustering resolution.

        """
        self.label_all=label_all
        self.classify_mode=classify_mode
        self.reorder_mode_x=reorder_mode_x
        self.bandwidth_estimate=bandwidth_estimate
        if bandwidth==0 and self.bandwidth==0:
            if self.bandwidth_estimate =='HaarWavelet':
                if sigma==0:
                    self.sigma=1.0
                else:
                    self.sigma=sigma
                self.bandwidth,self.hist_distribution,self.hist_bins,self.hist_binwidth, self.bandwidth_full= self.get_bandwidth_HaarWavelet(sigma=self.sigma,max_layer=max_layer,min_layer=min_layer,consecutive_fail=consecutive_fail)
            else:
                if sigma==0:
                    self.sigma=0.0092
                else:
                    self.sigma=sigma
                self.bandwidth,self.hist_distribution,self.hist_bins,self.hist_binwidth, self.bandwidth_full= self.get_bandwidth_Gaussian(sigma=self.sigma,accelerate=accelerate)
        elif bandwidth!=0:
            if sigma==0:
                self.sigma=0.0092
            else:
                self.sigma=sigma
            self.bandwidth=bandwidth

        ms = MeanShift(bandwidth=self.bandwidth, bin_seeding=False,cluster_all=True, max_iter=800)
        groupID_all=ms.fit_predict(self.t_embedded)
        self.cluster_centers=ms.cluster_centers_
        self.groupnum=self.cluster_centers.shape[0]

        if self.classify_mode=='ClusterCenter':
            # Assign the group label to cloest cluster centers
            if self.label_all!='Both':
                self.t_gID=self.MSC_classify(self.t_embedded)
                self.reorder_groupID(mode_x=self.reorder_mode_x)
                self.f_gID=self.MSC_classify(self.f_embedded)
            else:
                self.t_gID,_=self.MSC_classify(self.t_embedded)
                self.reorder_groupID(mode_x=self.reorder_mode_x)
                self.f_gID,_=self.MSC_classify(self.f_embedded)
        elif self.classify_mode=='TrainingData':
            self.t_gID=self.reformat_groupID_pocket(groupID_all)
            self.reorder_groupID(mode_x=self.reorder_mode_x)
            if self.label_all!='Both':
                self.f_gID=self.MSC_classify(self.f_embedded)
            else:
                self.f_gID,_=self.MSC_classify(self.f_embedded)

        if self.text_out:
            print(f'MSC Bandwidth {self.bandwidth}')
            print(f'Number of Groups: {self.groupnum}')
        return None

    def MSC_classify(self,n_embedded,dist_in=np.array([]),multibandwidth=False,multiscales=np.array([])):
        """
        Classifies new environments using Mean Shift Clustering.

        This method assigns new data points (embedded structures) to existing clusters 
        based on their distances to cluster centers. If no precomputed distances are provided, 
        they will be calculated within the function. The method can also support multi-bandwidth 
        and multi-scale clustering.

        Parameters
        ----------
        n_embedded : numpy.ndarray
            Embedded data of the new environments, denoted as \chi'. The array should be 
            of shape `(num_samples, dimension)`, where `num_samples` is the number of new 
            data points and `dimension` is the dimensionality of the embedded space.
            Indices should be in the format `[atomID, dimension]`.
        dist_in : numpy.ndarray, optional
            Precomputed distances between `n_embedded` and the cluster centers. If not provided, 
            distances will be computed in this function. The default is an empty array.
        multibandwidth : bool, optional
            If `True`, enables the use of multiple bandwidths for clustering. Default is `False`.
        multiscales : numpy.ndarray, optional
            Array of scaling factors for multi-scale clustering. The default is an empty array.

        Returns
        -------
        n_gID : list of lists
            Member list of new atom IDs in each group. Each entry corresponds to a group, 
            with a list of atom IDs belonging to that group. For example, `n_gID[2] = [5, 7, 10]` 
            means that GroupID 2 contains atom IDs 5, 7, and 10 in the new environment.
            The last entry (`n_gID[-1]`) contains the atom IDs that are distinct and do not 
            belong to any group.

        Notes
        -----
        The classification process assigns each new data point to the closest cluster center 
        based on the provided or computed distances. Multi-bandwidth and multi-scale options 
        can be used to adjust the clustering behavior.
        """
        # Computet the matrix of squared pairwise distances (D^2) between embedded SOAP
        # in incoming structure(s) and cluster center obtained
        if self.classify_mode=='ClusterCenter':
            if dist_in.shape[0]==0:
                dist_list=cdist(n_embedded,self.cluster_centers)
            else:
                dist_list=dist_in
            n_gID=dist_list.argsort(axis=1)[:,0]
            n_gID=[np.where(n_gID==gID)[0] for gID in np.arange(self.groupnum)]
        elif self.classify_mode=='TrainingData':
            if dist_in.shape[0]==0:
                dist_list=cdist(n_embedded,self.t_embedded)
            else:
                dist_list=dist_in

            if not(multibandwidth):
                # reformat_the_pocket_into_list, then take the groupID of the training data closest to inputs
                n_gID=self.reformat_groupID(self.t_gID)[dist_list.argsort(axis=1)[:,0]]
                n_gID=[np.where(n_gID==gID)[0] for gID in np.arange(self.groupnum)]
            else:
                dist_list=np.multiply(dist_list,np.tile(1/multiscales,(dist_list.shape[0],1)))
                n_gID=self.reformat_groupID(self.t_gID)[dist_list.argsort(axis=1)[:,0]]
                n_gID=[np.where(n_gID==gID)[0] for gID in np.arange(self.groupnum)]
        else:
            print('Unknown choice of classify mode.')
            exit()

        if (self.label_all=='LabelAll') or (self.label_all=='Both'):
            n_gID_label_all=n_gID
            n_gID_label_all=n_gID_label_all+[np.empty((0),dtype=int)]
        if (self.label_all=='LabelDistinct') or (self.label_all=='Both'):
            # Check the distinctness of incoming SOAP from the existing dataset
            if self.classify_mode!='TrainingData':
                dist_list=cdist(self.t_embedded,n_embedded)
                if not(multibandwidth):
                    distinct=np.where([not(True in (dist_list[:,item]<=self.bandwidth)) for item in np.arange(dist_list.shape[1])])[0]
                else:
                    distinct=np.where([not(True in (dist_list[:,item]<=1.0)) for item in np.arange(dist_list.shape[1])])[0]
            else:
                dist_list=dist_list.T
                if not(multibandwidth):
                    distinct=np.where([not(True in (dist_list[:,item]<=self.bandwidth)) for item in np.arange(dist_list.shape[1])])[0]
                else:
                    distinct=np.where([not(True in (dist_list[:,item]<=1.0)) for item in np.arange(dist_list.shape[1])])[0]
            n_gID= [np.setdiff1d(item, distinct) for item in n_gID]+[distinct]

        if self.label_all=='Both':
            return (n_gID, n_gID_label_all)
        elif (self.label_all=='LabelAll'):
            return n_gID_label_all
        elif (self.label_all=='LabelDistinct'):
            return n_gID
        else:
            return (n_gID, n_gID_label_all)

    def get_bandwidth_Gaussian(self,sigma=0.00920,accelerate=False):
        """
        Estimates the bandwidth for Mean Shift Clustering (MSC) using Gaussian Kernel Density Estimation.

        This method computes the bandwidth for MSC by applying Gaussian Kernel Density Estimation (KDE) 
        to the pairwise distances between all pairs of embedded SOAP data. It allows for an option 
        to reduce computational load by considering only a subset of the pairwise distances.

        Parameters
        ----------
        sigma : float
            The sigma_smear parameter used in the estimation, as described in the article [1]_. 
            This parameter controls the smoothing in the Gaussian KDE.
        accelerate : bool, optional
            If `True`, only the lowest 40% of the maximum pairwise distances are included 
            in the estimation, to reduce computation load when constructing the density distribution. 
            Default is `False`.

        Returns
        -------
        bandwidth : float
            The estimated bandwidth for MSC, denoted as \delta_MSC in the article [1]_.
        distribution : numpy.ndarray
            The approximated density at each bin location, which can be used for visualization.
        bins : numpy.ndarray
            The bin locations corresponding to the approximated density.
        binwidth : float
            The width of each bin used in the density approximation.
        bandwidth_full : list of float
            A list of length scale candidates for the bandwidth. The first value 
            (`bandwidth_full[0]`) is the chosen bandwidth.

        Notes
        -----
        Gaussian Kernel Density Estimation (KDE) is used to approximate the density distribution 
        of the pairwise distances. The `accelerate` option can be used to limit the number of 
        pairwise distances considered, thereby speeding up the computation at the expense 
        of some accuracy.

        """
        pd_metric=pdist(self.t_embedded, 'euclidean')
        maxpd=np.max(pd_metric)
        if accelerate:
            pd_metric=pd_metric[pd_metric<=0.4*maxpd]
        # Kernel Density Estimation of the distribution of pairwise distance
        kde = KernelDensity(kernel='gaussian', bandwidth=sigma).fit(pd_metric[:, np.newaxis])

        # For computtaional efficiency, minimum (thus bandwidth) is obtained with
        # the data binned with binwidth \sigma_smear / (2*gaussian_resol+1)
        # e.g. For a dataset with only one pair distance,
        #      the Gaussian is approximated with sigma/41
        gaussian_resol=10
        binwidth=sigma/(2*gaussian_resol+1)
        binnum=np.ceil(maxpd/binwidth).astype('int')
        maxbin=binwidth*binnum
        bins = np.linspace(0,maxbin, num=binnum)

        distribution=np.exp(kde.score_samples(bins[:, np.newaxis]))
        distribution=distribution
        # If the minimum is numerically flat, it takes the left side of it
        # Index: next bin smaller
        hill1=np.where((distribution[1:]-distribution[:-1])<0)[0]
        if hill1.shape[0]>0:
            hill1=hill1+1
        # Index: next bin larger or equal
        hill2=np.where((distribution[1:]-distribution[:-1])>=0)[0]
        min_index=np.intersect1d(hill1,hill2)

        # The index where there is a gap between this and next bin
        # gap_index = np.where((bins[1:]-binwidth-bins[:-1])>binwidth/2)[0]
        gap_index = np.where((bins[1:]-binwidth*1.05-bins[:-1])>0)[0]
        if gap_index.shape[0]>0:
            gap_distribution=np.zeros((gap_index.shape[0]))
            gap_bandwidth=(bins[gap_index]+binwidth+bins[gap_index+1])/2
            min_index=np.setdiff1d(min_index,gap_index)
        min_distribution=distribution[min_index]
        min_bandwidth = (bins+binwidth/2)[min_index]
        if gap_index.shape[0]>0:
            bandwidth_full=np.concatenate((min_bandwidth,gap_bandwidth))
        else:
            bandwidth_full=min_bandwidth

        bandwidth_full= bandwidth_full[bandwidth_full.argsort()]
        bandwidth_full = bandwidth_full[bandwidth_full>0]
        if bandwidth_full.shape[0]==0:
            bandwidth_full=np.array([maxpd])
        bandwidth=bandwidth_full[0]
        return bandwidth,distribution,bins,binwidth,bandwidth_full

    def get_bandwidth_HaarWavelet(self,sigma=1.0, max_layer=10, min_layer=0, consecutive_fail=1):
        """
        Estimates the bandwidth for Mean Shift Clustering (MSC) using Haar wavelet expansion.

        This method computes the bandwidth for MSC by applying Haar wavelet expansion 
        to the pairwise distances between all pairs of embedded SOAP data. The expansion 
        stops when the variance of the approximation relative to the coefficients falls 
        below a specified threshold (`sigma`), or when a maximum layer of the wavelet 
        expansion is reached.

        Parameters
        ----------
        sigma : float, optional
            Threshold for the variance of the approximation relative to the coefficient 
            to stop splitting a bin. Default is 1.0.
        max_layer : int, optional
            The maximum degree of the Haar wavelet expansion. If set to 0, no limit is imposed. 
            Default is 10.
        min_layer : int, optional
            The minimum degree of the Haar wavelet expansion. Default is 0.
        consecutive_fail : int, optional
            Number of consecutive failures allowed during the approximation process. Default is 1.

        Returns
        -------
        bandwidth : float
            The estimated bandwidth for MSC, denoted as \delta_MSC in the article [1].
        distribution : numpy.ndarray
            The approximated density at each bin location, useful for visualization.
        bins : numpy.ndarray
            The bin locations corresponding to the approximated density.
        binwidth : float
            The bin width used for the density approximation.
        bandwidth_full : list of float
            A list of length scale candidates for the bandwidth. The first value 
            (`bandwidth_full[0]`) is the chosen bandwidth.

        Notes
        -----
        The Haar wavelet expansion is used to approximate the density distribution of the 
        pairwise distances. The expansion is refined iteratively until the specified criteria 
        are met (based on `sigma`, `max_layer`, and `consecutive_fail`). This approach provides 
        a method for adaptively estimating the clustering bandwidth.

        """
        print(f'Started Estimation of Bandwidth with Haar Wavelet {sigma}')
        pd_metric=np.sort(pdist(self.t_embedded, 'euclidean'))
        maxpd=pd_metric[-1]
        data_norm=1/pd_metric.shape[0]
        wave_norm=1/np.sqrt(maxpd)
        const_coe=wave_norm*data_norm*pd_metric.shape[0]
        const_err=(wave_norm-const_coe*wave_norm)**2/pd_metric.shape[0]/(pd_metric.shape[0]-1)
        if self.text_out:
            print(f'{const_coe} {const_err/const_coe**2}')

        #    Construct the bins that are "active" (Ready to be divided to the next wavelet level)
        #    a-bin=[[index_0, index_1, index_2, n (wavelet order), k (bin location),consecutive fail]...]
        #    pd_metric[index_0:index_1]>=0
        #    pd_metric[index_1:index_2]<0
        #    The width of wavelet (binwidth)= maxpd/2**n
        #    The location of the bin in that nth order, k* maxpd/2**n
        a_bin= np.array([[0, np.sum(pd_metric<=(maxpd/2)), pd_metric.shape[0], 0, 0, 0]])
        #    Coefficient of the approximation
        a_coe= np.array([(2*a_bin[0,1]-a_bin[0,0]-a_bin[0,2])*data_norm*wave_norm*2**(a_bin[0,3]/2)])
        #    Error at each data
        #    variance(wavelet_(x_i)-coeficient)**2
        # a_err= np.array([((a_bin[0,1]-a_bin[0,0])*(a_coe[0]-wave_norm*np.sqrt(2**a_bin[0,3]))**2+(a_bin[0,2]-a_bin[0,1])*(a_coe[0]+wave_norm*np.sqrt(2**a_bin[0,3]))**2)/(a_bin[0,2]-a_bin[0,0])/(a_bin[0,2]-a_bin[0,0]-1)])
        a_app = np.array([data_norm-const_coe*wave_norm-a_coe[0]*wave_norm*np.array([1,-1])])
        a_err= np.array([(a_app[0,0]**2*(a_bin[0,1]-a_bin[0,0])+a_app[0,1]**2*(a_bin[0,2]-a_bin[0,1]))\
                            /(a_bin[0,2]-a_bin[0,0])/(a_bin[0,2]-a_bin[0,0]-1)])

        if self.text_out:
            # The criteria of stop iteration of a bin
            print(f'===Layer: {a_err/a_coe**2}')
            print(f'{a_coe} {a_err}')

        i_bin=np.empty((0,6))
        i_coe=np.empty((0))
        i_err=np.empty((0))
        i_app=np.empty((0,2))

        h_bin=a_bin
        h_coe=a_coe
        h_err=a_err
        h_app=a_app

        loop_iter=0
        while True:
            if a_bin.shape[0]==0:
                if self.text_out:
                    print('no active bin left')
                break
            #    Divide each active bin to next level n
            #    temporary bin [[indices_0,indices_2, n, k]]
            t_bin= np.vstack([[[item[0], item[1], item[3]+1, item[4]*2, item[5]], [item[1], item[2], item[3]+1, item[4]*2+1, item[5]]] for item in a_bin])
            bin_filter=(t_bin[:,1]-t_bin[:,0])!=0
            t_bin=t_bin[bin_filter]
            t_app= np.concatenate(a_app)
            t_app = t_app[bin_filter]
            #    Screen the bins with zero members
            #    temporary bin [[indices_0,indices_1, indices_2, n, k]]
            if 0 in [2**item[2] for item in t_bin]:
                x=np.where(np.array([2**item[2] for item in t_bin]) ==0)[0]
                print(np.where(np.array([2**item[2] for item in t_bin]) ==0)[0])
                print(t_bin[x])
            t_bin= np.vstack(([t_bin[:,0]], [t_bin[:,0]+np.array([np.sum(pd_metric[item[0]:item[1]]*(2**item[2])<= ((item[3]+0.5)*maxpd)) for item in t_bin])], t_bin[:,1:].T)).T
            bin_filter=(t_bin[:,2]-t_bin[:,0])!=0
            t_bin= t_bin[bin_filter]
            t_app = t_app[bin_filter]
            #    Screen the bin that are empty
            #    Get the coefficients of each bin
            t_coe= np.multiply((2*t_bin[:,1]-t_bin[:,0]-t_bin[:,2])*data_norm*wave_norm,2**(t_bin[:,3]/2))

            # t_err= ((t_bin[:,1]-t_bin[:,0])*(t_coe-wave_norm*np.sqrt(2**t_bin[:,3]))**2+(t_bin[:,2]-t_bin[:,1])*(t_coe+wave_norm*np.sqrt(2**t_bin[:,3]))**2)
            # t_err=np.array([item/(t_bin[id,2]-t_bin[id,0])/(t_bin[id,2]-t_bin[id,0]-1) if (t_bin[id,2]-t_bin[id,0])>1 else 1 for (id,item) in enumerate(t_err)])
            temp_app=t_coe*wave_norm*2**(t_bin[:,3]/2)
            t_app=np.vstack([t_app-temp_app,t_app+temp_app]).T
            t_err= ((t_bin[:,1]-t_bin[:,0])*t_app[:,0]**2+(t_bin[:,2]-t_bin[:,1])*t_app[:,1]**2)
            t_err=np.array([item/(t_bin[id,2]-t_bin[id,0])/(t_bin[id,2]-t_bin[id,0]-1) if (t_bin[id,2]-t_bin[id,0])>1 else 1 for (id,item) in enumerate(t_err)])

            drop_bin= np.where(t_coe==0)[0]
            if drop_bin.shape[0]>0:
                i_bin=np.vstack((i_bin,t_bin[drop_bin]))
                i_coe=np.concatenate((i_coe,t_coe[drop_bin]))
                i_err=np.concatenate((i_err,t_err[drop_bin]))
                i_app=np.concatenate((i_app,t_app[drop_bin]))

                keep_bin= np.setdiff1d(np.arange(t_bin.shape[0]), drop_bin)
                t_bin=t_bin[keep_bin]
                t_coe=t_coe[keep_bin]
                t_err=t_err[keep_bin]
                t_app=t_app[keep_bin]

            drop_bin= np.where(((t_err/t_coe**2) >=sigma))[0]

            drop2=np.where(t_bin[:,3]==63)[0]
            if drop2.shape[0]>0:
                drop_bin= np.unique(np.concatenate((drop_bin,drop2))).astype('int')

            keep_bin= np.setdiff1d(np.arange(t_bin.shape[0]),drop_bin)
            t_bin[drop_bin,5]+=1
            t_bin[keep_bin,5]=0
            # drop_bin= np.where((t_err) >=threshold)[0]
            drop_bin= np.where(t_bin[:,5]>=consecutive_fail)[0]
            if self.text_out:
                print(f'===Layer {loop_iter}: {t_err/t_coe**2}')
                print(f'Num in bin {t_bin[:,2]-t_bin[:,0]}')
                print(f'Consecutuve Failing {t_bin[:,5]}')
            if drop_bin.shape[0]>0 and loop_iter>=min_layer:
                # Bins which are not included in expension
                i_bin=np.vstack((i_bin,t_bin[drop_bin]))
                i_coe=np.concatenate((i_coe,t_coe[drop_bin]))
                i_err=np.concatenate((i_err,t_err[drop_bin]))
                i_app=np.concatenate((i_app,t_app[drop_bin]))

                # All bins involved in expension
                keep_bin= np.setdiff1d(np.arange(t_bin.shape[0]), drop_bin)
                h_bin=np.vstack((h_bin,t_bin[keep_bin]))
                h_coe=np.concatenate((h_coe,t_coe[keep_bin]))
                h_err=np.concatenate((h_err,t_err[keep_bin]))
                h_app=np.concatenate((h_app,t_app[keep_bin]))

                # Latest active bins
                a_bin=t_bin[keep_bin]
                a_coe=t_coe[keep_bin]
                a_err=t_err[keep_bin]
                a_app=t_app[keep_bin]
            else:
                # All bins need further expension
                h_bin=np.vstack((h_bin,t_bin))
                h_coe=np.concatenate((h_coe,t_coe))
                h_err=np.concatenate((h_err,t_err))
                h_app=np.concatenate((h_app,t_app))

                a_bin=t_bin
                a_coe=t_coe
                a_err=t_err
                a_app=t_app

            loop_iter+=1
            if max_layer!=0:
                if loop_iter>=max_layer:
                    if self.text_out:
                        print(f'Expension degree reaching limit {max_layer}')
                    break


        i_bin=i_bin[i_bin[:,0].argsort()].astype('int')

        binwidth=maxpd/2**i_bin[:,3]
        distribution=(i_bin[:,2]-i_bin[:,0])/binwidth/pd_metric.shape[0]
        bins=binwidth*i_bin[:,4]

        if bins.shape[0]==0:
            print(f'NO Bandwidth exists in this dataset!')
            print(f'range of seperation {maxpd}')
            return maxpd,np.array([pd_metric.shape[0]/maxpd/2]),np.array([maxpd]),np.array([maxpd*2]),np.array([[maxpd,maxpd*2]])       

        # Index: next bin smaller
        hill1=np.where((distribution[1:]-distribution[:-1])<0)[0]
        if hill1.shape[0]>0:
            hill1=hill1+1
        # Index: next bin larger or equal
        hill2=np.where((distribution[1:]-distribution[:-1])>0)[0]

        v_index= [hill1[hill1<=item] for item in hill2]
        v_index= np.array([[item[-1],up_in] for up_in,item in zip(hill2,v_index) if  item.shape[0]>0])
        if v_index.shape[0]!=0:
            v_index = np.split(v_index, np.where((v_index[1:,0]-v_index[:-1,0])>0)[0]+1)
            v_index = np.array([item[0] for item in v_index])
        gap_index = np.where((bins[1:]-bins[:-1]-1.05*binwidth[:-1])>0)[0]
        if gap_index.shape[0]>0:
            gap_bandwidth=(bins[gap_index]+binwidth[gap_index]+bins[gap_index+1])/2
            gap_distribution = np.zeros((gap_index.shape[0]))
            min_index=np.array([item for item in v_index if not(False in [(item2<item[0]) or (item2>item[1]) for item2 in gap_index])])
        elif v_index.shape[0]>0:
            min_index=v_index
        else:
            min_index=np.array([])
        
        if min_index.shape[0]>0:
            min_bandwidth = ((bins+binwidth/2)[min_index[:,1]] + (bins+binwidth/2)[min_index[:,0]])/2
        else:
            min_bandwidth =np.array([])
        
        if gap_index.shape[0]>0:
            bandwidth_full=np.concatenate((min_bandwidth,gap_bandwidth))
        else:
            bandwidth_full=min_bandwidth

        bandwidth_full= bandwidth_full[bandwidth_full.argsort()]
        bandwidth_full = bandwidth_full[bandwidth_full>0]
        if bandwidth_full.shape[0]==0:
            bandwidth_full=np.array([maxpd])
        bandwidth=bandwidth_full[0]

        if self.text_out:
            print(f'Binnum {bins.shape[0]}, max {maxpd}, bandwidth {bandwidth}, binwidth{np.min(binwidth)}, {np.max(i_bin[:,3]-1)}')
        return bandwidth,distribution,bins,binwidth,bandwidth_full

    def get_HDBSCAN(self,min_cluster_size=2,min_samples=0,label_all='LabelAll',classify_mode='ClusterCenter'):
        """
        Performs clustering using HDBSCAN (Hierarchical Density-Based Spatial Clustering 
        of Applications with Noise) [3]_.

        This method applies the HDBSCAN algorithm to the data to identify clusters based on 
        density. It uses parameters to control the minimum cluster size and the minimum number 
        of samples per cluster. The method also supports different options for labeling data points 
        based on their proximity to cluster centers.

        Parameters
        ----------
        min_cluster_size : int, optional
            The minimum number of points required to form a cluster in HDBSCAN. Default is 2.
        min_samples : int, optional
            The minimum number of samples in a neighborhood for a point to be considered a 
            core point in HDBSCAN. If set to 0, it will be adjusted to `min_cluster_size`. 
            Default is 0.
        label_all : str, optional
            Specifies the method for labeling data points:
            - `'LabelDistinct'`: Assigns a data point as distinct if it has no neighbor 
            within the bandwidth radius from `.t_embedded`. This is the default mode.
            - `'LabelAll'`: Labels data points based on the nearest cluster center.
            - `'Both'`: Outputs both distinct and all labeled clusters as a tuple.
            Default is `'LabelAll'`.
        classify_mode : str, optional
            The mode used for classifying data points. Default is `'ClusterCenter'`.

        Returns
        -------
        None

        Attributes
        ----------
        groupnum : int
            The number of groups out of the clustering, is saved as attribute.
        f_gID : list ofnumpy.ndarray
            The group membership of each sample in the full dataset `f_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group. For example, `f_gID[2] = [5, 7, 10]` 
            means that group 2 contains atom IDs 5, 7, and 10 from `f_embedded`. The last list 
            (`f_gID[-1]` or `f_gID[groupnum]`) contains the atom IDs that do not belong to any group.
        t_gID : list ofnumpy.ndarray
            The group membership of each sample in the training set `t_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group in `t_embedded`. For example, 
            `t_gID[2] = [5, 7, 10]` means that group 2 contains atom IDs 5, 7, and 10 from `t_embedded`. 
            The last list (`t_gID[-1]` or `t_gID[groupnum]`) contains the atom IDs that do not belong to any group.

        Notes
        -----
        HDBSCAN is a density-based clustering algorithm that extends DBSCAN by converting it 
        into a hierarchical clustering algorithm. The `min_cluster_size` and `min_samples` 
        parameters control the density threshold for clusters.

        References
        ----------
        .. [3] L. McInnes, J. Healy, and S. Astels, “hdbscan: Hierarchical density based clustering.” J. Open Source Softw. 2, 205 (2017).
           URL : https://hdbscan.readthedocs.io/en/latest/index.html
        
        """
        self.label_all=label_all
        # self.classify_mode=classify_mode
        if min_samples==0:
            min_samples=min_cluster_size
        import hdbscan
        self.clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size,min_samples=min_samples, gen_min_span_tree=True,prediction_data=True)
        self.clusterer.fit(self.t_embedded)
        self.groupnum=int(np.max(self.clusterer.labels_))+1

        # Assign the group label to cloest cluster centers
        self.t_gID=self.HDBSCAN_classify(self.t_embedded)
        self.f_gID=self.HDBSCAN_classify(self.f_embedded)
        if self.text_out:
            print(f'HDBSCAN min.cluster_size, min.samples {min_cluster_size,min_samples}')
            print(f'Number of Groups: {self.groupnum}\nNumber of Noise Points {self.f_gID[-1].shape[0]}')
        return None

    def HDBSCAN_classify(self,n_embedded):
        """
        Classifies new environments using HDBSCAN[3]_ clustering.

        This method assigns new data points (embedded structures) to existing HDBSCAN 
        clusters based on their embedding in the feature space. It returns the membership 
        information for each data point, indicating which group (cluster) they belong to.

        Parameters
        ----------
        n_embedded : numpy.ndarray
            The embedded data representing the new environments, denoted as \chi'. 
            The array should have a shape of `(num_samples, dimension)`, where 
            `num_samples` is the number of new data points and `dimension` is the 
            dimensionality of the embedded space.
            Indices should be in the format `[atomID, dimension]`.

        Returns
        -------
        n_gID : list of lists
            A list of member lists, where each sublist corresponds to a group (cluster) 
            and contains the atom IDs of the members in that group. For example, 
            `n_gID[2] = [5, 7, 10]` means that GroupID 2 contains atom IDs 5, 7, and 10 
            in the new environment.
            The last sublist (`n_gID[-1]`) contains the atom IDs that are distinct 
            and do not belong to any group.

        Notes
        -----
        The classification assigns each new data point to the closest existing HDBSCAN 
        cluster center or marks it as distinct if it does not belong to any cluster.

        """
        raw_gID=self.HDBSCAN_classify_fork(self.clusterer,n_embedded)
        n_gID_label_all= [np.where(raw_gID[0]==item)[0] for item in np.arange(self.groupnum)]
        if (self.label_all=='LabelAll') or (self.label_all=='Both'):
            n_gID_label_all=n_gID_label_all+[np.empty((0),dtype=int)]

        if (self.label_all=='LabelDistinct') or (self.label_all=='Both'):
            distinct=np.where(raw_gID[0]==-1)[0]
            n_gID=[np.setdiff1d(item, distinct) for item in n_gID_label_all]+[distinct]

        if self.label_all=='Both':
            return (n_gID, n_gID_label_all)
        elif (self.label_all=='LabelAll'):
            return n_gID_label_all
        elif (self.label_all=='LabelDistinct'):
            return n_gID
        else:
            return (n_gID, n_gID_label_all)
        
    # """
    # ================================================================================
    # =================================Small Tools====================================
    # ================================================================================
    # """
    
    def kernel_dot(self,data_A,data_B):
        """
        The default choice of kernel of dot product for Kernel Principal Component Analysis (kPCA).

        This method calculates the kernel matrix for kPCA by computing the dot product 
        between each pair of samples from the input datasets A and B. The resulting matrix 
        represents the similarity between the samples in terms of their dot products.

        Parameters
        ----------
        data_A : numpy.ndarray
            The first dataset with shape `(num_samples_A, num_features)`, where 
            `num_samples_A` is the number of samples in dataset A, and `num_features` 
            is the number of features.
        data_B : numpy.ndarray
            The second dataset with shape `(num_samples_B, num_features)`, where 
            `num_samples_B` is the number of samples in dataset B, and `num_features` 
            is the number of features. The number of features in `data_A` and `data_B` 
            must be the same.

        Returns
        -------
        numpy.ndarray
            The kernel matrix representing the dot product between each pair of samples 
            from datasets A and B. The shape of the matrix is `(num_samples_A, num_samples_B)`.

        Notes
        -----
        The dot product kernel is the default kernel function used in kPCA, representing 
        the linear similarity between the samples in the input feature space.

        """
        return np.dot(data_A,data_B.T)

    # Broken-stick approach to estimate the suitable MDS dimension
    def get_broken_stick(self, segments):
        """
        Computes the broken-stick model for estimating the dimensionality of MDS/PCA/kPCA embedding.

        This method calculates the broken-stick model series, which is used to estimate 
        the appropriate number of dimensions to retain in dimensionality reduction techniques 
        like MDS, PCA, or kPCA. The broken-stick model provides a threshold for the significance 
        of eigenvalues by comparing them to the expected distribution of variance.

        Parameters
        ----------
        segments : int
            The number of segments the stick is broken into, representing the total 
            number of eigenvalues obtained from the dimensionality reduction method 
            (PCA, kPCA, or MDS).

        Returns
        -------
        None

        Attributes
        ----------
        broken_stick : numpy.ndarray
            The series of broken stick model.

        Notes
        -----
        The broken-stick model is used as a heuristic to identify significant components 
        in dimensionality reduction. It provides a way to estimate the number of dimensions 
        to retain by comparing the eigenvalues against a random distribution.

        """
        self.broken_stick=np.array([np.sum([1/item2 for item2 in np.arange(item,segments+1)])\
                                    for item in np.arange(1,segments+1)])/segments
        return None

    def reorder_groupID(self,mode_x=False):
        """
        Reorders group IDs in a more intuitive way.

        This method reassigns group indices to make them more human-readable by either 
        considering the mean location of each group or ordering based on the first component. 
        The reordering strategy depends on the value of `mode_x`.

        Parameters
        ----------
        mode_x : bool, optional
            Determines the method for reordering the group IDs:
            - If `False` (default), group IDs are assigned based on the "min location," 
            which is the minimum value of each SOAP component. GroupID 0 is assigned 
            to the cluster closest to this "min location," and subsequent group IDs 
            are assigned to the cluster closest to the previous one, based on the 
            mean location of members.
            - If `True`, group IDs are ordered based on the order of the first component 
            of each group.

        Returns
        -------
        None

        Attributes
        ----------
        cluster_centers :numpy.ndarray
            List of cluster centers in embedded space is saved as attribute.
        f_gID : list ofnumpy.ndarray
            The group membership of each sample in the full dataset `f_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group. For example, `f_gID[2] = [5, 7, 10]` 
            means that group 2 contains atom IDs 5, 7, and 10 from `f_embedded`. The last list 
            (`f_gID[-1]` or `f_gID[groupnum]`) contains the atom IDs that do not belong to any group.
        t_gID : list ofnumpy.ndarray
            The group membership of each sample in the training set `t_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group in `t_embedded`. For example, 
            `t_gID[2] = [5, 7, 10]` means that group 2 contains atom IDs 5, 7, and 10 from `t_embedded`. 
            The last list (`t_gID[-1]` or `t_gID[groupnum]`) contains the atom IDs that do not belong to any group.

        Notes
        -----
        This function is used to reorder clusters in a more intuitive manner, making 
        the group IDs easier to interpret. The reordering can be based on the spatial 
        arrangement of clusters or the values of the first component.

        """
        if self.cluster_str=='HDBSCAN':
            print('WARNING: Reordering groupID is not supported for HDBSCAN, no reordering is executed.')
            return None
        data_min=np.min(self.t_embedded,axis=0)
        data_max=np.max(self.t_embedded,axis=0)
        trans_soap=(self.t_embedded-np.tile(data_min,(self.t_embedded.shape[0],1)))/np.tile(data_max-data_min,(self.t_embedded.shape[0],1))
        positions=np.array([np.mean(trans_soap[self.t_gID[gid]],axis=0) for gid in np.arange(self.groupnum)])
        if mode_x:
            map_output=np.array([positions[:,0].argsort(),np.arange(positions.shape[0])]).T
        else:
            start_id=np.linalg.norm(positions,axis=1).argsort()[0]
            gid_map=np.array([start_id])
            pairwise=squareform(pdist(positions))
            reminding_gid=np.setdiff1d(np.arange(positions.shape[0]),gid_map)
            for gid in np.arange(positions.shape[0]-1):
                gid_map=np.concatenate((gid_map, [reminding_gid[pairwise[gid_map[-1]][reminding_gid].argsort()[0]]]))
                reminding_gid=np.setdiff1d(np.arange(positions.shape[0]),gid_map)
            map_output=np.array([gid_map,np.arange(positions.shape[0])]).T

        self.t_gID=[self.t_gID[item] for item in map_output[:,0]]+[self.t_gID[-1]]
        try:
            self.cluster_centers=np.array([self.cluster_centers[item] for item in map_output[:,0]])
        except:
            if self.cluster_str=='MSC':
                print(f'failed to reorder clusters IDs, cluster centers not exist?')
        return None

    def reformat_groupID(self, gID_pocket):
        """
        Converts the expression of group IDs.

        This method transforms a list of group memberships (groupID to atomID mapping) 
        into a format that provides the groupID for each individual atom. The input 
        specifies the members of each group, and the output is a list where each index 
        corresponds to an atom and contains its assigned groupID.

        Parameters
        ----------
        gID_pocket : list of lists
            A list of groups, where each sublist contains the atom IDs belonging to that 
            group. For example, `gID_pocket[2] = [5, 7, 10]` means that GroupID 2 contains 
            atom IDs 5, 7, and 10 in the new environment. The last sublist in the list 
            (`gID_pocket[-1]`) represents members that are distinct and do not belong to 
            any group.

        Returns
        -------
        gid_list : list of int
            A list where each element represents the groupID of the corresponding atom. 
            For example, `gid_list[0]` is the groupID of atom 0, `gid_list[1]` is the 
            groupID of atom 1, and so on.

        Notes
        -----
        This function is useful for converting the group membership expression from a 
        group-based format (group to members) to an atom-based format (atom to group).

        Examples
        --------
        >>> # Example usage of reformat_groupID
        >>> # Assuming embed_cluster is already defined
        >>> # embed_cluster.gID_pocket = [[0, 2, 4], [1, 3], [5, 7, 10], []]  Group 0 contains [0, 2, 4], etc.
        >>> gid_list = embed_cluster.reformat_groupID(embed_cluster.gID_pocket)
        >>> print(gid_list)
        >>> # Expected output gid_list = [0, 1, 0, 1, 0, 2, ...]
        # Output: List where each index represents the groupID of the corresponding atom

        """
        gID_list= [np.vstack((gID_pocket[item],np.full_like(gID_pocket[item],item))).T for item in np.arange(self.groupnum)]
        if (gID_pocket[-1].shape[0]!=0) and (len(gID_pocket)==self.groupnum+1):
            gID_list += [np.vstack((gID_pocket[-1],np.full_like(gID_pocket[-1],-1))).T]
        gID_list= np.vstack(gID_list)
        gID_list=gID_list[gID_list[:,0].argsort(),1]
        return gID_list

    def reformat_groupID_pocket(self, gID_list):
        """
        Converts the expression of group IDs to a pocket format.

        This method transforms a list of group assignments for each atom into a format 
        that groups atoms by their assigned group IDs. The input specifies the group 
        ID of each atom, and the output is a list where each index corresponds to a 
        group and contains the atom IDs belonging to that group. An additional list 
        at the end contains atom IDs that are considered distinct, ungroupable, or noise.

        Parameters
        ----------
        gID_list : list of int
            A list where each element represents the group ID of the corresponding atom. 
            For example, `gID_list[2] = 3` means that atom ID 2 belongs to group 3. 
            A value of `-1` indicates that the atom is distinct, noise, or ungroupable.

        Returns
        -------
        gID_pocket : list of numpy.ndarray
            A list of member lists, where each sublist corresponds to a group and contains 
            the atom IDs belonging to that group. For example, `gID_pocket[2] = [5, 7, 10]` 
            means that GroupID 2 contains atom IDs 5, 7, and 10 in the new environment.
            The last sublist (`gID_pocket[-1]`) contains atom IDs that are distinct or ungroupable.

        Notes
        -----
        This function is useful for converting the group membership expression from an 
        atom-based format (atom to group) to a group-based format (group to members), 
        facilitating the organization of atoms by their assigned group IDs.

        Examples
        --------
        >>> # Example usage of reformat_groupID_pocket
        >>> # Assuming embed_cluster is already defined
        >>> gID_list = [0, 1, 3, 0, 3, -1, 2, 3]  # AtomID 0 and 3 belong to Group 0, etc.
        >>> gID_pocket = model.reformat_groupID_pocket(gID_list)
        >>> print(gID_pocket)
        >>> # Expected output: [[0, 3], [1], [6], [2, 4, 7], [5]]
        # Output: List of lists, where each sublist represents the members of a group

        """
        gID_pocket = [np.where(gID_list==item)[0] for item in np.arange(self.groupnum)]
        gID_pocket += [np.empty((0))]
        if -1 in gID_list:
            gID_pocket[-1]=np.where(gID_list==-1)[0]
        return gID_pocket

    def manual_group(self,gID_split=np.empty((0)),pocket_split=[]):
        """
        ***EXPERIMENTAL FUNCTION***
        Manually updates the grouping by splitting or merging groups.

        This function allows the user to manually modify the clustering results by 
        splitting or merging groups based on external information not captured by 
        the descriptors. This can be useful for incorporating additional knowledge 
        such as statistical data from multiple DFT calculations or structural searches.

        Parameters
        ----------
        gID_split : list of int, optional
            A list of group IDs to be split. For example, `[2, 4, 10]` means that 
            Group 2, Group 4, and Group 10 are going to be split. Default is an empty array.
            
        pocket_split : list of list of lists, optional
            The expected grouping result for the specified groups to be split. 
            Each entry corresponds to a group being split, formatted as a list of 
            "pockets" representing the new subgroups within that original group.
            For example, `[[[1, 3, 5], [4, 6, 8]], [pockets for Group 4], [pockets for Group 10]]` 
            indicates that Group 2 (first entry) will be split into two subgroups: 
            `[1, 3, 5]` and `[4, 6, 8]`. The numbers refer to data IDs from the training 
            data (`self.t_data` or `self.t_embedded`).

            - All data IDs from the original group must be accounted for in the new pockets.
            - Each subsequent list entry follows the same structure for the other groups specified in `gID_split`.

        Returns
        -------
        None

        Attributes
        ----------
        groupnum : int
            The number of groups out of the clustering, is saved as attribute.
        cluster_centers :numpy.ndarray
            List of cluster centers in embedded space is saved as attribute.
        f_gID : list ofnumpy.ndarray
            The group membership of each sample in the full dataset `f_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group. For example, `f_gID[2] = [5, 7, 10]` 
            means that group 2 contains atom IDs 5, 7, and 10 from `f_embedded`. The last list 
            (`f_gID[-1]` or `f_gID[groupnum]`) contains the atom IDs that do not belong to any group.
        t_gID : list ofnumpy.ndarray
            The group membership of each sample in the training set `t_data`. Each entry in the list is 
            a list of atom IDs that belong to the corresponding group in `t_embedded`. For example, 
            `t_gID[2] = [5, 7, 10]` means that group 2 contains atom IDs 5, 7, and 10 from `t_embedded`. 
            The last list (`t_gID[-1]` or `t_gID[groupnum]`) contains the atom IDs that do not belong to any group.

        Notes
        -----
        This function allows for manual adjustments to the clustering, enabling users to 
        refine the groups with additional domain-specific information that may not be 
        captured by the descriptors. 

        CAUTION: When using this function, the behavior of subsequent grouping under 
        classification mode `cluster` is not guaranteed.

        Examples
        --------
        >>> # Example usage of manual_group
        >>> gID_split = [2, 4, 10]  # Groups to be split
        >>> pocket_split = [
        ...     [[1, 3, 5], [4, 6, 8]],  # Splitting Group 2
        ...     [[2, 7], [9, 11]],       # Splitting Group 4
        ...     [[0, 13], [14, 15]]      # Splitting Group 10
        ... ]
        >>> model = MyClusteringClass()  # Assuming MyClusteringClass contains the manual_group method
        >>> model.manual_group(gID_split=gID_split, pocket_split=pocket_split)

        """
        if self.cluster_str!='MSC':
            print(f'This function is only available for meanshift clustering model, not for '+self.cluster_str)
            exit()
            return None

        groupID_children=np.ones((self.groupnum))
        groupID_children[gID_split]=np.array([len(item) for item in pocket_split])
        gID_mapping=np.cumsum(groupID_children).astype('int')-1
        new_groupnum=np.max(gID_mapping).astype('int')+1

        new_cluster=np.zeros((new_groupnum,self.cluster_centers.shape[1]))
        new_cluster[gID_mapping]=self.cluster_centers
        new_t_gID=[[] for item in np.arange(new_groupnum)]

        new_gIDs=[np.arange(gID_mapping[item-1]+1,gID_mapping[item]+1) if item!=0 else np.arange(gID_mapping[0]+1) for item in gID_split]
        for gID in np.arange(self.groupnum):
            if not(gID in gID_split):
                new_t_gID[gID_mapping[gID]]= self.t_gID[gID]
            else:
                which_pocket=np.where(gID_split==gID)[0][0]
                safe_guard= np.concatenate(pocket_split[which_pocket])
                not_in_pocket= np.setdiff1d(self.t_gID[gID],safe_guard)
                if not_in_pocket.shape[0]!=0:
                    print(f'Some of the members in the splitting group are undecided where to go')
                    return None
                not_in_group = np.setdiff1d(safe_guard,self.t_gID[gID])
                if not_in_group.shape[0]!=0:
                    print(f'Some of the member in pocket are not in the group to split')
                    return None
                for (pocket_order,extra_gID) in enumerate(new_gIDs[which_pocket]):
                    new_t_gID[extra_gID]= pocket_split[which_pocket][pocket_order]
                    new_t_gID[extra_gID]=new_t_gID[extra_gID][new_t_gID[extra_gID].argsort()]
                new_cluster[new_gIDs[which_pocket]]=np.array([np.mean(self.t_embedded[new_t_gID[extra_gID]],axis=0) for (pocket_order,extra_gID) in enumerate(new_gIDs[which_pocket])])

        self.t_gID=new_t_gID
        self.cluster_centers=new_cluster
        self.groupnum=new_groupnum
        print(f'New group num: {self.groupnum}')
        if self.classify_mode=='ClusterCenter':
            # Assign the group label to cloest cluster centers
            if self.label_all!='Both':
                self.t_gID=self.MSC_classify(self.t_embedded)
                self.reorder_groupID(mode_x=self.reorder_mode_x)
                self.f_gID=self.MSC_classify(self.f_embedded)
            else:
                self.t_gID,_=self.MSC_classify(self.t_embedded)
                self.reorder_groupID(mode_x=self.reorder_mode_x)
                self.f_gID,_=self.MSC_classify(self.f_embedded)
        elif self.classify_mode=='TrainingData':
            if self.label_all!='Both':
                self.t_gID=self.MSC_classify(self.t_embedded)
                self.reorder_groupID(mode_x=self.reorder_mode_x)
                self.f_gID=self.MSC_classify(self.f_embedded)
            else:
                self.t_gID,_=self.MSC_classify(self.t_embedded)
                self.reorder_groupID(mode_x=self.reorder_mode_x)
                self.f_gID,_=self.MSC_classify(self.f_embedded)
        return None

    def get_db_plottings(self, export='png', folder_path='./', folder_name='PlotDump', toplot='All', color_code={} ):
        """
        A utility for plotting various aspects of the database.

        This function provides tools for visualizing data related to the database, such as 
        eigenvalues, bandwidth histograms, and embedded data points. The user can choose 
        the export format, folder location for saving the plots, and the specific content to plot.

        Parameters
        ----------
        export : str or bool, optional
            Specifies the output format for the images. Options are:
            - `'png'`: Export images as `.png` files.
            - `'pdf'`: Export images as `.pdf` files.
            - `'png+pdf'`: Export images in both `.png` and `.pdf` formats.
            - `False`: Display the plot on screen using `plt.show()`. Default is `'png'`.
        folder_path : str, optional
            The base path for creating the folder where images will be saved. Default is `'./'`.
        folder_name : str, optional
            The name of the folder to store the exported images. The full path will be 
            `folder_path + folder_name`. Default is `'PlotDump'`.
        toplot : str, optional
            A string specifying the content to be plotted. Multiple options can be combined 
            using a `+` sign. Available options include:
            - `'Eigen'`: Plot eigenvalues from PCA, kPCA, or MDS.
            - `'Bandwidth'`: Plot a histogram from the KDE of pairwise distances to locate 
            bandwidth for MSC.
            - `'Embedded'`: Plot embedded data points.
            - `'Embedded+TrainingData'`: Plot only `.t_embedded` data points, excluding `.f_embedded`.
            - `'Embedded+IndividualHighlight'`: Plot each group individually in a subfolder 
            inside `folder_path + folder_name`.
            - `'Monochrome'`: Plot all groups in black, ignoring any color codes.
            - `'All'`: Equivalent to plotting `'Embedded+IndividualHighlight+Bandwidth'`.

            Default is `'All'`. Additional combinations can be made, e.g., `'All+TrainingData'`.
        color_code : dict, optional
            A dictionary specifying the RGBA color for each group. For example, 
            `color_code[1]` contains the RGBA values for group 1.

        Returns
        -------
        None


        Attributes
        ----------
        color_code : dict
            Color code is saved as an attribute

        Notes
        -----
        This function facilitates various ways to visualize data related to the database. 
        It supports different plotting options, such as visualizing eigenvalues, bandwidth 
        histograms, and embedded data points, with flexible export formats.

        """
        import matplotlib.pyplot as plt
        from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
        import matplotlib.colors as mcolors
        from matplotlib import cm
        from matplotlib.colors import ListedColormap, LinearSegmentedColormap, Normalize

        if self.text_out:
            print(f'Just for initial visualization, options for color schemes and groups e.g. best choice of color scheme')
        if export!=False:
            if folder_path[-1]!='/':
                folder_path+='/'
            folder=folder_path+folder_name
            try:
                os.mkdir(folder)
            except:
                pass
        else:
            export=''
        if ('Eigen' in toplot) or 'All' in toplot:
            if  self.embed_str=='MDS' or (self.embed_str=='kPCA' or self.embed_str=='PCA'):
                if self.text_out:
                    print('Plotting eigenvalues in log scale of the embedding problem '+self.embed_str)
                file_name = self.embed_str +'_Eigenvalue'
                # plot the explained eigenvalues
                fig,ax = plt.subplots(figsize=(3.54,3.54), dpi=600)
                plt.rc('xtick', labelsize=10)
                plt.rc('ytick', labelsize=10)
                fig.patch.set_facecolor('white')
                plt.xlabel(f"a", fontsize=20)
                plt.ylabel(f"$\lambda_a/\Sigma_k^N \lambda_k$", fontsize=20)
                plt.yscale('log')
                eval_output=np.max([5,2+self.t_embedded.shape[1]])
                # If the dimension was defined by user then the broken stick model will not be plotted
                try:
                    plt.scatter(np.arange(self.evals[:eval_output].shape[0])+1,self.broken_stick[:eval_output],color='gray',s=15,marker='o',zorder=2)
                    plt.plot(np.arange(self.evals[:eval_output].shape[0])+1,self.broken_stick[:eval_output],color='gray',linewidth=1,zorder=1)
                except:
                    pass
                plt.scatter(np.arange(self.evals[:eval_output].shape[0])+1,self.evals[:eval_output]/np.sum(self.evals),color='blue',s=15,marker='d',zorder=2)
                plt.plot(np.arange(self.evals[:eval_output].shape[0])+1,self.evals[:eval_output]/np.sum(self.evals),color='black',linewidth=1,zorder=1)
                fig.tight_layout()
                # Export the file or show it
                if 'pdf' in export:
                    plt.savefig(os.sep.join([folder,file_name+'.pdf']), bbox_inches='tight')
                    if self.text_out:
                        print('Exported: '+os.sep.join([folder,file_name+'.pdf']))
                if 'png' in export:
                    plt.savefig(os.sep.join([folder,file_name+'.png']), bbox_inches='tight')
                    if self.text_out:
                        print('Exported: '+os.sep.join([folder,file_name+'.png']))
                if export=='':
                    plt.show()
                plt.close()

        if ('Embedded' in toplot) or ('All' in toplot):
            if self.text_out:
                print('Plotting '+self.embed_str+ '-Embedded Data.')
                print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                print('For a humane way to read,\ngroup ID in the file names start from 1\ngroupID in the class starts from 0')
                print('Embedded components in the file names start from 1\nEmbedded components in the class starts from 0')
                print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            file_name = self.embed_str +'_Embedded'
            if 'TrainingData' in toplot:
                embedded=self.t_embedded
                gID=self.t_gID
            else:
                embedded=self.f_embedded
                gID=self.f_gID

            if 'Monochrome' in toplot:
                print('Color Mode: Monochrome, all groups are black in color')
                # All groups are black
                self.color_code={item: np.array([0,0,0,0])/255 for item in np.arange(self.groupnum)}
            elif color_code!={}:
                print('Color Mode: Provded color code')
                # Color code provided
                self.color_code=color_code
            else:
                print('Color Mode: Automatic. .color_code[groupID] gives the rgba for a groupID in the image')
                print('')
                # Generate a color code for each group, in a linear color map
                norm = Normalize(vmin=-0.5, vmax=self.groupnum-0.5)
                mapclass=cm.ScalarMappable(norm=norm, cmap='jet')
                self.color_code={item: mapclass.to_rgba(item) for item in np.arange(self.groupnum)}

            dim_pair=np.array([[item,item+1] for item in np.arange(0,embedded.shape[1],2)])
            if dim_pair[-1,-1]==embedded.shape[1]:
                dim_pair[-1,1]=dim_pair[-1,0]
                dim_pair[-1,0]=0
            for ploting_pair in dim_pair:
                fig,ax  = plt.subplots(figsize=(3.54,3.54), dpi=600)
                fig.patch.set_facecolor('white')
                plt.xlabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[0]+1)), fontsize=20)
                plt.ylabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[1]+1)), fontsize=20)

                if gID[-1].shape[0]!=0:
                    plt.scatter(embedded[gID[-1],ploting_pair[0]],embedded[gID[-1],ploting_pair[1]],color='black',marker='x',label='Distinct',alpha=0.5)
                for groupID,atomID in enumerate(gID[:-1]):
                    plt.scatter(embedded[atomID,ploting_pair[0]],embedded[atomID,ploting_pair[1]],color=self.color_code[groupID],label='Group '+str(groupID),s=10)

                fig.tight_layout()
                # Export the file or show it
                if 'pdf' in export:
                    plt.savefig(os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.pdf']), bbox_inches='tight')
                    if self.text_out:
                        print('Exported: '+os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.pdf']))
                if 'png' in export:
                    plt.savefig(os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.png']), bbox_inches='tight')
                    if self.text_out:
                        print('Exported: '+os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.png']))
                if export=='':
                    plt.show()
                plt.close()

            if ('IndividualHighlight' in toplot) or ('All' in toplot):
                if self.text_out:
                    print('Extra output highlighting one group per image is exported')
                if ('png' in export) or ('pdf' in export):
                    subfolder=folder+'/Individual'
                    try:
                        os.mkdir(subfolder)
                    except:
                        pass
                file_name = self.embed_str +'_Embedded'
                for ploting_pair in dim_pair:
                    for groupID,atomID in enumerate(gID[:-1]):
                        fig = plt.figure(figsize=(3.54,3.54), dpi=600)
                        fig.patch.set_facecolor('white')
                        plt.xlabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[0]+1)), fontsize=20)
                        plt.ylabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[1]+1)), fontsize=20)
                        plt.scatter(embedded[:,ploting_pair[0]],embedded[:,ploting_pair[1]],color='black',label='Group '+str(groupID),s=10)
                        plt.scatter(embedded[atomID,ploting_pair[0]],embedded[atomID,ploting_pair[1]],color=self.color_code[groupID],label='Group '+str(groupID),s=10)
                        fig.tight_layout()
                        if 'pdf' in export:
                            plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.pdf']), bbox_inches='tight')
                            if self.text_out:
                                print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.pdf']))
                        if 'png' in export:
                            plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.png']), bbox_inches='tight')
                            if self.text_out:
                                print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.png']))
                        if export=='':
                            plt.show()
                        plt.close()
                    if gID[-1].shape[0]!=0:
                        fig = plt.figure(figsize=(3.54,3.54), dpi=600)
                        fig.patch.set_facecolor('white')
                        plt.xlabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[0]+1)), fontsize=20)
                        plt.ylabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[1]+1)), fontsize=20)
                        plt.scatter(embedded[:,ploting_pair[0]],embedded[:,ploting_pair[1]],color='black',label='Group '+str(groupID),s=10)
                        plt.scatter(embedded[gID[-1],ploting_pair[0]],embedded[gID[-1],ploting_pair[1]],color='black',marker='x',label='Distinct',alpha=0.5)
                        fig.tight_layout()
                        if 'pdf' in export:
                            plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.pdf']), bbox_inches='tight')
                            if self.text_out:
                                print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.pdf']))
                        if 'png' in export:
                            plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.png']), bbox_inches='tight')
                            if self.text_out:
                                print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.png']))
                        if export=='':
                            plt.show()
                        plt.close()

            if self.cluster_str=='MSC':
                if ('Bandwidth' in toplot) or ('All' in toplot):
                    file_name = self.embed_str +'_Bandwidth'
                    try:
                        fig, ax = plt.subplots(dpi=600,figsize=(3.54*2,3.54))
                        fig.patch.set_facecolor('white')
                        ax.tick_params(axis='x',direction='out')
                        ax.minorticks_off()
                        ax.xaxis.set_ticks_position('bottom')
                        plt.bar((self.hist_bins)+self.hist_binwidth/2,self.hist_distribution, width = self.hist_binwidth,color=np.array([41,72,93,255])/255,zorder=1)

                        if ('FullBandwidth' in toplot):
                            for item in self.bandwidth_full:
                                plt.plot([item,item],[0,1.02*np.max(self.hist_distribution)],color=np.array([198,211,37,255])/255,zorder=0)
                        else:
                            plt.plot([self.bandwidth,self.bandwidth],[0,1.02*np.max(self.hist_distribution)],color=np.array([198,211,37,255])/255,zorder=0)
                        # plt.xlabel("D", fontsize=20)
                        # plt.ylabel(r"$\rho(D)$", fontsize=20)
                        plt.xlabel("Pairwise Separation", fontsize=20)
                        plt.ylabel("Density Function", fontsize=20)
                        fig.tight_layout()
                        if 'pdf' in export:
                            plt.savefig(os.sep.join([folder,file_name+f'.pdf']), bbox_inches='tight')
                            if self.text_out:
                                print('Exported: '+os.sep.join([folder,file_name+f'.pdf']))
                        if 'png' in export:
                            plt.savefig(os.sep.join([folder,file_name+f'.png']), bbox_inches='tight')
                            if self.text_out:
                                print('Exported: '+os.sep.join([folder,file_name+f'.png']))
                        if export=='':
                            plt.show()
                        plt.close()
                    except:
                        print('exception here (exporting bandwidth)')
                        pass

        if self.text_out:
            print('The End of Plotting Routine')
        return None

    def get_embed_plottings(self, n_embedded, n_gID, export='png', folder_path='./', folder_name='PlotDump', toplot='All', color_code={} ):
        """
        A utility for plotting embedded data.

        This function provides tools for visualizing embedded data by plotting groups based 
        on their group IDs. It allows for flexible export options, folder locations for saving 
        the plots, and specific plotting requests. Unlike `get_db_plottings`, this method focuses 
        solely on `.t_gID` and `.t_embedded`.

        Parameters
        ----------
        n_embedded : numpy.ndarray
            The embedded data to be plotted, typically obtained from an embedding process, 
            such as `.embed()`. The shape should be `(num_samples, dimensions)`, where 
            `num_samples` is the number of data points, and `dimensions` is the number 
            of dimensions in the embedded space.
        n_gID : list of int
            The group IDs corresponding to the embedded data points, typically obtained 
            from a classification process, such as `.classify()`. The length should match 
            the number of samples in `n_embedded`.
        export : str or bool, optional
            Specifies the output format for the images. Options are:
            - `'png'`: Export images as `.png` files.
            - `'pdf'`: Export images as `.pdf` files.
            - `'png+pdf'`: Export images in both `.png` and `.pdf` formats.
            - `False`: Display the plot on the screen using `plt.show()`. Default is `'png'`.
        folder_path : str, optional
            The base path for creating the folder where images will be saved. Default is `'./'`.
        folder_name : str, optional
            The name of the folder to store the exported images. The full path will be 
            `folder_path + folder_name`. Default is `'PlotDump'`.
        toplot : str, optional
            A string specifying the content to be plotted. Available options include:
            - `'IndividualHighlight'`: Plot each group individually in a subfolder 
            inside `folder_path + folder_name`.
            - `'Monochrome'`: Plot all groups in black, ignoring any color codes.
            - `'All'`: Equivalent to `'IndividualHighlight'`.

            Default is `'All'`, which is equivalent to `'IndividualHighlight'`.
        color_code : dict, optional
            A dictionary specifying the RGBA color for each group. For example, 
            `color_code[1]` contains the RGBA values for group 1. If not provided, 
            default colors will be used.

        Returns
        -------
        None

        Notes
        -----
        This function is used for plotting embedded data points with different visualization 
        options. It is primarily focused on `.t_gID` and `.t_embedded`, providing flexibility 
        for individual or monochrome group visualizations.

        """
        import matplotlib.pyplot as plt
        from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
        import matplotlib.colors as mcolors
        from matplotlib import cm
        from matplotlib.colors import ListedColormap, LinearSegmentedColormap, Normalize
        if self.text_out:
            print('Plotting '+self.embed_str+ '-Embedded Data.')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('For a humane way to read,\ngroup ID in the file names start from 1\ngroupID in the class starts from 0')
            print('Embedded components in the file names start from 1\nEmbedded components in the class starts from 0')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        file_name = self.embed_str +'_Embedded_OutOfSample'
        if self.text_out:
            print(f'Just for initial visualization, options for color schemes and groups e.g. best choice of color scheme')
        if export!=False:
            if folder_path[-1]!='/':
                folder_path+='/'
            folder=folder_path+folder_name
            try:
                os.mkdir(folder)
            except:
                pass
        else:
            export=''

        embedded=self.t_embedded
        gID=self.t_gID

        if 'Monochrome' in toplot:
            print('Color Mode: Monochrome,   all groups are black in color')
            # All groups are black
            color_code={item: np.array([0,0,0,0])/255 for item in np.arange(self.groupnum)}
        elif color_code!={}:
            print('Color Mode: Provded color code')
            # Color code provided
            pass
        else:
            print('Color Mode: Automatic. .color_code[groupID] gives the rgba for a groupID in the image')
            print('')
            # Generate a color code for each group, in a linear color map
            norm = Normalize(vmin=-0.5, vmax=self.groupnum-0.5)
            mapclass=cm.ScalarMappable(norm=norm, cmap='jet')
            color_code={item: mapclass.to_rgba(item) for item in np.arange(self.groupnum)}

        dim_pair=np.array([[item,item+1] for item in np.arange(0,embedded.shape[1],2)])
        if dim_pair[-1,-1]==embedded.shape[1]:
            dim_pair[-1,1]=dim_pair[-1,0]
            dim_pair[-1,0]=0
        for ploting_pair in dim_pair:
            fig,ax  = plt.subplots(figsize=(3.54,3.54), dpi=600)
            fig.patch.set_facecolor('white')
            plt.xlabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[0]+1)), fontsize=20)
            plt.ylabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[1]+1)), fontsize=20)

            if n_gID[-1].shape[0]!=0:
                plt.scatter(n_embedded[n_gID[-1],ploting_pair[0]],n_embedded[n_gID[-1],ploting_pair[1]],color='black',marker='x',label='Distinct',s=2,alpha=0.25)

            for groupID,atomID in enumerate(n_gID[:-1]):
                plt.scatter(n_embedded[atomID,ploting_pair[0]],n_embedded[atomID,ploting_pair[1]],color=color_code[groupID],s=3,alpha=0.25)

            for groupID,atomID in enumerate(gID[:-1]):
                plt.scatter(embedded[atomID,ploting_pair[0]],embedded[atomID,ploting_pair[1]],color=color_code[groupID],label='Group '+str(groupID),s=3,marker='v',linewidth=0.1,edgecolor= "black")
            # Export the file or show it
            if 'pdf' in export:
                plt.savefig(os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.pdf']), bbox_inches='tight')
                if self.text_out:
                    print('Exported: '+os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.pdf']))
            if 'png' in export:
                plt.savefig(os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.png']), bbox_inches='tight')
                if self.text_out:
                    print('Exported: '+os.sep.join([folder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}.png']))
            if export=='':
                plt.show()
            plt.close()

        if ('IndividualHighlight' in toplot) or 'All' in toplot:
            if self.text_out:
                print('Extra output highlighting one group per image is exported')
            if ('pdf' in export) or ('png' in export):
                subfolder=folder+'/Individual'
                try:
                    os.mkdir(subfolder)
                except:
                    pass
                file_name = self.embed_str +'_Embedded'
            for ploting_pair in dim_pair:
                for groupID,atomID in enumerate(gID[:-1]):
                    fig = plt.figure(figsize=(3.54,3.54), dpi=600)
                    fig.patch.set_facecolor('white')
                    plt.xlabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[0]+1)), fontsize=20)
                    plt.ylabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[1]+1)), fontsize=20)
                    plt.scatter(embedded[:,ploting_pair[0]],embedded[:,ploting_pair[1]],color='black',label='Group '+str(groupID),s=10,alpha=0.25,marker='v')
                    plt.scatter(n_embedded[n_gID[groupID],ploting_pair[0]],n_embedded[n_gID[groupID],ploting_pair[1]],color=color_code[groupID],label='Group '+str(groupID),s=10)
                    plt.scatter(embedded[atomID,ploting_pair[0]],embedded[atomID,ploting_pair[1]],color=color_code[groupID],label='Group '+str(groupID),s=10,alpha=0.25,marker='v',linewidth=1)
                    if 'pdf' in export:
                        plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.pdf']), bbox_inches='tight')
                        if self.text_out:
                            print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.pdf']))
                    if 'png' in export:
                        plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.png']), bbox_inches='tight')
                        if self.text_out:
                            print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Group_{groupID+1}.png']))
                    if export=='':
                        plt.show()
                    plt.close()
                if n_gID[-1].shape[0]!=0:
                    fig = plt.figure(figsize=(3.54,3.54), dpi=600)
                    fig.patch.set_facecolor('white')
                    plt.xlabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[0]+1)), fontsize=20)
                    plt.ylabel(r"$\mathregular{\chi'_{s1}}$".replace('s1', str(ploting_pair[1]+1)), fontsize=20)
                    plt.scatter(n_embedded[n_gID[-1],ploting_pair[0]],n_embedded[n_gID[-1],ploting_pair[1]],color='black',marker='x',label='Distinct',alpha=0.5)
                    plt.scatter(embedded[:,ploting_pair[0]],embedded[:,ploting_pair[1]],color='black',label='Group '+str(groupID),s=10)
                    if 'pdf' in export:
                        plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.pdf']), bbox_inches='tight')
                        if self.text_out:
                            print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.pdf']))
                    if 'png' in export:
                        plt.savefig(os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.png']), bbox_inches='tight')
                        if self.text_out:
                            print('Exported: '+os.sep.join([subfolder,file_name+f'_{ploting_pair[0]+1}_{ploting_pair[1]+1}_Distinct.png']))
                    if export=='':
                        plt.show()
                    plt.close()
        if self.text_out:
            print('The End of Plotting Routine')
        return None
