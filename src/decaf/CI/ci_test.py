from os import path
import decaf
import numpy as np
from ase import io as aseio
import os

# Check function reading the structure
def test_ase_read():
    folder='./examples/Structures'
    filenames=['PAH_'+item+'.con' for item in ['benzene','naphthalene','anthracene','tetracene','phenanthren','graphene']]
    structures=[aseio.read(os.sep.join([folder,item])) for item in filenames]
    structures[-1].pbc=[True,True,False]
    assert len(structures)==6

# Check function computing SOAPs
def test_soap_calculation():
    folder='./examples/Structures'
    filenames=['PAH_'+item+'.con' for item in ['benzene','naphthalene','anthracene','tetracene','phenanthren','graphene']]
    structures=[aseio.read(os.sep.join([folder,item])) for item in filenames]
    structures[-1].pbc=[True,True,False]
    SOAP_L_nmax=4
    SOAP_L_lmax=3
    SOAP_S_nmax=8
    SOAP_S_lmax=4
    # All length scale as an input is normalized with nearest neighbor distances
    # Cut-off of short range SOAP, in the middle of 1st and 2nd coordination shell of carbon in graphene
    SOAP_S_cut=(1+np.sqrt(3))/2
    SOAP_S_sigma=SOAP_S_cut/8
    # Cut-off of short range SOAP, in the middle of 3rd and 4th coordination shell of carbon in graphene
    SOAP_L_cut=(2+3)/2
    SOAP_L_sigma=SOAP_L_cut/8
    
    target_IDs= [[0],[4,5],[1,2],[1,2,5],[0],[1]]
    # Obtain the full SOAP of the structures
    All_SOAP=np.concatenate([decaf.get_SOAP(item,1.42,species=[6,1],\
                                        SOAP_L_nmax=SOAP_L_nmax,\
                                        SOAP_L_lmax=SOAP_L_lmax,\
                                        SOAP_S_nmax=SOAP_S_nmax,\
                                        SOAP_S_lmax=SOAP_S_lmax,\
                                        SOAP_S_cut=SOAP_S_cut,SOAP_S_sigma=SOAP_S_sigma,SOAP_L_cut=SOAP_L_cut,SOAP_L_sigma=SOAP_L_sigma,\
                                        periodic=(True in item.pbc),atomID=np.array(target_IDs[ids])) for ids,item in enumerate(structures)])
    
    assert All_SOAP.shape[0]==np.concatenate(target_IDs).shape[0]
    All_SOAP=np.concatenate([decaf.get_SOAP(item,1.42,species=[6,1],\
                                        SOAP_L_nmax=SOAP_L_nmax,\
                                        SOAP_L_lmax=SOAP_L_lmax,\
                                        SOAP_S_nmax=SOAP_S_nmax,\
                                        SOAP_S_lmax=SOAP_S_lmax,\
                                        SOAP_S_cut=SOAP_S_cut,SOAP_S_sigma=SOAP_S_sigma,SOAP_L_cut=SOAP_L_cut,SOAP_L_sigma=SOAP_L_sigma,\
                                        periodic=(True in item.pbc)) for ids,item in enumerate(structures)])
    return All_SOAP

# A test to dry run the PAH database with heuristics 
def test_EC_obj_dry_heurestics():    
    # Obtain the full SOAP of the structures
    All_SOAP=test_soap_calculation()
    PAH_EC= decaf.embed_cluster(All_SOAP, embed_str='MDS',max_training_size=10000, cluster_str='MSC', text_out=False)
    PAH_EC.workflow(dim=0,bandwidth_estimate='HaarWavelet',reorder_mode_x=False)
    assert PAH_EC.groupnum<13


# A test to dry run the PAH database without heuristics 
def test_EC_workflow_input_param():
    # Obtain the full SOAP of the structures
    All_SOAP=test_soap_calculation()
    PAH_EC= decaf.embed_cluster(All_SOAP, embed_str='MDS',max_training_size=10000, cluster_str='MSC', text_out=False)
    PAH_EC.workflow(dim=2,bandwidth=0.6,bandwidth_estimate='HaarWavelet',reorder_mode_x=False)
    assert PAH_EC.groupnum==3
    return All_SOAP, PAH_EC

# A test chaeck the consistency of the workflow vs embedding
def test_EC_workflow_embed_consistency():
    All_SOAP, PAH_EC_W = test_EC_workflow_input_param()

    PAH_EC_S= decaf.embed_cluster(All_SOAP, embed_str='MDS',max_training_size=10000, cluster_str='MSC', text_out=False)
    PAH_EC_S.get_embed(dim=2)
    
    check_diff= np.linalg.norm(PAH_EC_W.t_embedded-PAH_EC_S.t_embedded,axis=1)
    
    assert np.max(check_diff)<0.001
    return All_SOAP, PAH_EC_W, PAH_EC_S

# A test chaeck the consistency of the workflow vs clustering
def test_EC_workflow_cluster_consistency():
    # Obtain the full SOAP of the structures
    All_SOAP, PAH_EC_W, PAH_EC_S = test_EC_workflow_embed_consistency()
    PAH_EC_S.get_cluster(bandwidth=0.6)
    concat_grouping_W= np.concatenate(([PAH_EC_W.groupnum],np.concatenate(PAH_EC_W.t_gID)))
    concat_grouping_S= np.concatenate(([PAH_EC_S.groupnum],np.concatenate(PAH_EC_S.t_gID)))
    
    assert not (False in ((concat_grouping_W-concat_grouping_S)==0))
