.. _tutorial_embedding:

Embeding the Training Dataset
=============================

Embedding Operation and input
-----------------------------

After initializing the class object ``decaf.embed_cluster(...)``, we can proceed constructing an embedding space for the ``training_set``.
The corresponding functions would be wrapped with the function inside the class ``.get_embed(...)``.

While the arguments of ``decaf.embed_cluster(...)`` depends on the choice of method, we will focus on classical MDS, PCA and kPCA on this page.
For these embedding methods, there are two arguments. Further elaboration is given below the list.

    * ``dim``: The dimension of the embedded space. Default is 0, for an auto detection with broken stick model.
    * ``less_stick_seg``: A boolean. In the case of ``dim==0``, this option allows a slightly stricter choice of ``dim``.

``dim``:

.. toggle::

    The parameter ``dim`` specifies the number of components the user aims to retain. 
    Notably, the embedding operation unavoidably truncates some information from the high-dimensional descriptors, so the choice of ``dim`` should adapt to the statistics of the training_set. 
    DECAF provides an initial guess for dim by setting ``dim=0``, which applies a broken-stick model that treats variance as if a stick is randomly broken into segments equal to the number of features, thereby modeling random noise data. 
    This method establishes a threshold, filtering dimensions with significance below that of a noise model.


``less_stick_seg``:

.. toggle::

    While the broken-stick model is commonly used in PCA, where the 'stick' is broken into segments of the number of features, ``M``, 
    its extension to MDS accounts for the maximum number of embedding dimensions being the number of data points in the training_set, ``N``. 
    In general, ``N`` may be larger or smaller than ``M``; however, the embedding space typically requires no more than ``min(N, M)`` dimensions to represent the dataset effectively. 
    The option ``less_stick_seg=True`` adjusts the broken-stick model to divide the stick into ``min(N, M)`` segments.

Attributes Saved in ``decaf.embed_cluster``
-------------------------------------------

After ``.get_embed(...)``, results and attributes will be saved as the following list, instead of through ``return``.

    * ``.dim``:
        The dimension of the embedding, will be the chosen number of dimension if estimated with broken-stick model. (NOT 0)
    * ``.t_embedded``:
        The embedded data in ``.dim`` dimensions.
        Dimensionally reduced data (embedded) for the training set ``t_data``.
        It is a ``numpy.ndarray`` in shape ``(.t_data.shape[0], .dim)``.
    * ``.embed_op``:
        An ``numpy.ndarray`` used as an operator for embedding. The shape of it and how to use it depend on which method you choose.
    * ``.evals``:
        The eigenvalues obtained in the PCA, kPCA or cMDS procedures.
        The eigenvalues are saved in descending order.
    * ``.evec`` :
        The COLUMNs are the eigenvectors obtained in the PCA, kPCA or cMDS procedures.
        The order of eigenvectors corresponds to that in ``.evals``.

Example
-------

Following the example in :ref:`Descriptor Construction <tutorial_descriptor>`, we will perform PCA embedding of the double-SOAP descriptor of the ``training_set``.

.. code-block:: python
    :linenos:

    # Initalizing the main class object with PCA
    embed_cluster = decaf.embed_cluster(training_set,embed_str='PCA')
    # Setting the dimension estimatin on by dim=0
    embed_cluster.get_embed(dim=0,less_stick_seg=False)

Results of the embedding:

.. code-block:: python
    :linenos:

    print(f'Number of Embedding Dimensions {embed_cluster.dim}')
    print(f'First 5 Eigenvalues\n{embed_cluster.evals[:5]}')
    print(f'Sanity check of orthogonality between eigenvectors')
    # The COLUMNs in .evec are the eigenvectors
    # Check the dot products between all pairs of eigenvectors
    orthogonality_check = np.dot(embed_cluster.evec.T, embed_cluster.evec)
    print(f'The diagonal are all essentially one')
    print(np.unique([orthogonality_check[i,i] for i in np.arange(orthogonality_check.shape[0])]))
    orthogonality_check = np.array([np.roll(row,-rowID) for (rowID,row) in enumerate(orthogonality_check)])[:,1:]
    print(f'The other elements are essentially zero')
    print(f'min: {np.min(orthogonality_check)} ; max: {np.max(orthogonality_check)}')

Output:

.. code-block::

    Number of Embedding Dimensions 3
    First 5 Eigenvalues
    [7.31469363e-01 3.42344673e-02 6.58527210e-03 2.44678754e-03
    1.46067719e-04]
    Sanity check of orthogonality between eigenvectors
    The diagonal are all essentially one
    [1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
    1. 1. 1. 1. 1. 1. 1.]
    The other elements are essentially zero
    min: -1.339206523454095e-15 ; max: 1.1926223897340549e-15

Other queries:

.. code-block:: python
    :linenos:

    # The embedded descriptor of ID=16,21 in ``training_set`` (or ``.t_data``)
    embed_cluster.t_embedded[[16,21]]

Combined with the :ref:`book keeping <tutorial_bookkeeping>`, 
one can query the embeded descriptor based on structure IDs and atom IDs.

.. image:: figures/PAH_naphthalene.png
    :width: 90%
    :align: center

The atoms in naphthalene (structure ID 1), atoms of ID 4 and 9 should be the same:

.. code-block:: python
    :linenos:
    
    mask1= training_set_origin_df['StructureID'] == 1
    ID1 = training_set_origin_df[mask1 & (training_set_origin_df['AtomID'] == 4)].index[0]
    ID2 = training_set_origin_df[mask1 & (training_set_origin_df['AtomID'] == 9)].index[0]
    print(f'The corresponding IDs are {ID1}, {ID2}')
    print(f'The difference between them in embedded space is essentially 0')
    print(np.linalg.norm(embed_cluster.t_embedded[ID1] - embed_cluster.t_embedded[ID2]))

Output

.. code-block::

    The corresponding IDs are 16, 21
    The difference between them in embedded space is essentially 0
    6.539213615042172e-14

The ``.t_embedded`` in a scatter plot: Each dot corresponds to one data point in ``.t_embedded``

.. code-block:: python
    :linenos:

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,2,figsize=(3.54*2,3.54))
    fig.patch.set_facecolor('white')
    ax[0].set_xlabel("Embedded Dimension 1", fontsize=16)
    ax[0].set_ylabel(r"Embedded Dimension 2", fontsize=16)
    # The first two dimension
    ax[0].scatter(embed_cluster.t_embedded[:,0],embed_cluster.t_embedded[:,1], color='k')
    # Dimension 1 and 3
    ax[1].set_xlabel("Embedded Dimension 1", fontsize=16)
    ax[1].set_ylabel(r"Embedded Dimension 3", fontsize=16)    
    ax[1].scatter(embed_cluster.t_embedded[:,0],embed_cluster.t_embedded[:,2], color='k')
    fig.tight_layout()
    plt.show()
    plt.close()

.. image:: figures/embedded_monochrome.png
    :width: 90%
    :align: center