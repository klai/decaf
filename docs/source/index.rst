.. DECAF documentation master file, created by
   sphinx-quickstart on Tue Oct  8 14:17:10 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DECAF Documentation
===================

DECAF stands for Descriptor Embedding and Clustering for Atomisitic-environment Framework, is a package for categorizing vectoral datasets into groups.


As a short introduction, the methodology is spelt out in the name. The workflow is outlined below:

| D: Descriptor

   | User provides a dataset of vectorial descriptors (e.g. SOAP[2_] or MACE[3_]).

| E: Embedding

   | The descriptor is usually high dimensional for efficient clustering, so we embed the descriptors into a lower dimensional space with multidimensional scaloing (MDS) or principal component analysis (PCA).

| C: Clusterung

   | The embedded data points are then clustered with an unsupervised clustering algorithm, our recommended and default choice is mean shift clustering (MSC).

| A: Atomisitic-environment

   | The package is desinged as a workflow to treat vertorial descriptor describing atomisitic-environments, user may instead input other vectorial datasets, perform embedding and clustering, however, please make sure all data points have the same number of features.

| F: Framework

   | DECAF is a versitile framework, you may replace components, like replacing SOAP descriptor with MACE descriptor, or using PCA instead of MDS.

Besides clustering a provided training dataset, DECAF is designed also for out-of-sample clustering. Users can provide data point(s) outside of the training set (consistent in descriptor setting of course) and identify which group these data point(s) belongs to, or if these data point(s) even belong to a group.


***************************************
Repository
***************************************

The package is available in this repository `https://gitlab.mpcdf.mpg.de/klai/decaf <https://gitlab.mpcdf.mpg.de/klai/decaf>`_

***************************************
Installation
***************************************
0. The following dependencies are required for this package:

   ::

      ase==3.23.0
      dscribe==2.1.1
      matplotlib==3.9.1
      numpy>=2.0.0
      scikit_learn>=1.5.1
      scipy>=1.13.1

1. Clone the repository with submodules

   .. code-block:: sh

      cd $HOME
      git clone --recurse-submodules git@gitlab.mpcdf.mpg.de:klai/decaf.git


2. Install the package

   Navigate into the directory where you just cloned.

   .. code-block:: sh

      cd $HOME/decaf
      pip install . -e --user

3. Import the package
   
   Actually importing the package in Python to use it.

   .. code-block:: python

      import decaf

***************************************
Journal Article
***************************************

Please cite the following article, when you refer to DECAF in your publication.

.. [1] K. C. Lai, S. Matera, C. Scheurer, K. Reuter, "A Fuzzy Classification Framework to Identify Equivalent Atoms in Complex Materials and Molecules" J. Chem. Phys 159.2 (2023). `DOI: 10.1063/5.0160369 <https://doi.org/10.1063/5.0160369>`_

.. [2] Albert P. Bartók, Risi Kondor, and Gábor Csányi. On representing chemical environments. Phys. Rev. B 87, 184115 (2013). `doi:10.1103/PhysRevB.87.184115 <https://doi.org/10.1103/PhysRevB.87.184115>`_

.. [3] Ilyes Batatia, Dávid Péter Kovács, Gregor N. C. Simm, Christoph Ortner, Gábor Csányi. MACE: Higher Order Equivariant Message Passing Neural Networks for Fast and Accurate Force Fields. Advances in Neural Information Processing Systems 35, 11423 (2022). `doi:10.48550/arXiv.2206.07697 <https://doi.org/10.48550/arXiv.2206.07697>`_

***************************************
Recent Changes
***************************************

v1.0.0:

- The first version uploaded. The performance is essentially the same as it was described in the journal aritcle [1]_ .

***************************************
License
***************************************

This work is licensed under a Creative Commons Attribution 4.0 International License.

`http://creativecommons.org/licenses/by/4.0/ <http://creativecommons.org/licenses/by/4.0/>`_

.. toctree::
   :maxdepth: 3
   :caption: Tutorials:
   
   tutorial

.. toctree::
   :maxdepth: 3
   :caption: Modules:
   
   decaf   

