# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import os
import sys
sys.path.insert(0, os.path.abspath('../../src'))

project = 'DECAF'
copyright = '2024, K. C. Lai, S. Matera, C. Scheurer, K. Reuter, Fritz Haber Institute - Max-Planck-Gesellschaft'
author = 'K. C. Lai, S. Matera, C. Scheurer, K. Reuter'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',  # Parses Numpy/Google style docstrings
    'sphinx_togglebutton',
]



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']
napoleon_google_docstring = False  # Disable Google style if you use only NumPy style
napoleon_numpy_docstring = True    # Enable NumPy style
napoleon_include_init_with_doc = False
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = True
napoleon_use_ivar = True
napoleon_use_param = True
napoleon_use_rtype = False
napoleon_preprocess_types = False
napoleon_type_aliases = None
napoleon_attr_annotations = True

html_logo = "../../DECAF_logo.png"
html_favicon = "../../DECAF_logo.png"

html_css_files = [
    'custom.css',  # Include the custom CSS to force dark mode
]