.. _tutorial_out_of_sample:

Out-of-sample Classification
============================

As a classifier, a DECAF model can be applied to structures from outside of the training set (out-of-sample classification). 
This is useful when scanning through a large data bank of pre-computed structures or in a generative workflow which expands the dataset of structures.
We will refer the new structures ``new_structures`` in examples below for convenience.

The out-of-classificaiton procedure is listed below. 
It follows the footsteps of previous pages of tutorials correspondingly, but applies an established DECAF model on new structures instead of constructing it.

    1. Descriptor construction
    2. Embedding
    3. Classification

Descriptor Construction
-----------------------
.. toggle ::

    Exactly the same procedure applied on the training set as on the page :ref:`Descriptor Construction <tutorial_descriptor>`.

    .. important::
        The descriptor settings (e.g. cut-off radii of SOAP) should be identifical to that applied on ``training_set``. 
        Typical descriptors (e.g. SOAP, MACE ...) is not comparable across different hyper-parameters.

Example (Descriptor)
^^^^^^^^^^^^^^^^^^^^
.. toggle ::

    Continue the example done in :ref:`Clustering <tutorial_clustering>`, 
    in which the DECAF model was established with three molecular structures (benzene, naphthalene, anthracene). 
    We would like to apply this DECAF model on other two structures: phenanthren and tetracene.
    We first construct the descriptors for them

    .. code-block:: python
        :linenos:

        # Typical procedures to load in the ASE Atoms from files, here we are using .con,
        # We are not mixing the list of structures for training_set and the out-of-sample set
        new_filenames=['PAH_phenanthren.con','PAH_tetracene.con']
        new_structures=[aseio.read(filename) for filename in new_filenames]

        # The following settings were used when establishing the DECAF model,
        # We are not changing them when constructing the descriptors for new_structures
        # lengthscale = 1.42
        # SOAP_S_cut=(1+np.sqrt(3))/2
        # SOAP_S_sigma=SOAP_S_cut/8
        # SOAP_S_nmax=8
        # SOAP_S_lmax=4
        # SOAP_L_cut=(2+3)/2
        # SOAP_L_sigma=SOAP_L_cut/8
        # SOAP_L_nmax=4
        # SOAP_L_lmax=3
        # species=np.unique(np.concatenate([struct.numbers for struct in structures])).astype('int')

        # Obtain the full SOAP of the new structures
        # Also speicify if the structures are periodic (False in this case)
        out_of_sample_set=np.concatenate([decaf.get_SOAP(struct,lengthscale,species=species,\
                                            SOAP_S_cut=SOAP_S_cut,SOAP_S_sigma=SOAP_S_sigma,\
                                            SOAP_S_nmax=SOAP_S_nmax, SOAP_S_lmax=SOAP_S_lmax,\
                                            SOAP_L_cut=SOAP_L_cut,SOAP_L_sigma=SOAP_L_sigma,\
                                            SOAP_L_nmax=SOAP_L_nmax, SOAP_L_lmax=SOAP_L_lmax,\
                                            periodic=False) for struct in new_structures])

    This procedures is exactly the same as that applied on ``structures`` to calculate the descriptors ``training_set`` on :ref:`Descriptor Construction <tutorial_descriptor>`.

Example (Book Keeping)
^^^^^^^^^^^^^^^^^^^^^^
.. toggle ::

    Like before, this set of descriptors is the only thing the embedding and clustering algorithms will see. 
    So once again, we need book keeping. It is the same as on :ref:`Book Keeping Tutorial <tutorial_bookkeeping>`.

    .. code-block:: python
        :linenos:

        out_of_sample_origin = np.vstack([np.vstack((np.full((len(struct)),structid),np.arange(len(struct)))).T for structid,struct in enumerate(new_structures)])
        out_of_sample_origin_df = pd.DataFrame({'StructureID': out_of_sample_origin[:, 0], 'AtomID': out_of_sample_origin[:, 1]})
    
    .. important::
        We do not mix the record ``training_set_origin_df`` in the previous example, becasue ``.index`` is directly the ID in ``training_set``. 
        One can construct alternatively a dataframe with extra columns to label whether it is involved in ``training_set`` and the ID there. 
        The implementation is a natural extension of this demonstration.

    In this ``out_of_sample_origin_df``, the ``.index`` is the ID corresponds to the order in ``out_of_sample_set``.

    .. image:: figures/PAH_phenanthren.png
        :width: 90%
        :align: center

    .. image:: figures/PAH_tetracene.png
        :width: 90%
        :align: center

    Some examples of queries:

    .. code-block:: python
        :linenos:

        # To query the origins of data of IDs e.g. [3,6,25,30,34], in the out_of_sample_set
        print(out_of_sample_origin_df.iloc[[3,6,25,30,34]])

        # Get the structures of these IDs
        print(f'The Structure IDs array')
        print(np.array(out_of_sample_origin_df.iloc[[3,6,25,30,34]]['StructureID']))

        # Get the structures of these IDs
        print(f'\nThe Atom IDs array')
        print(np.array(out_of_sample_origin_df.iloc[[3,6,25,30,34]]['AtomID']))

        # Get the out of sample datum IDs in out_of_sample_set from conditions
        print(f'\nThe IDs in out_of_sample_set of StructureID==1 and AtomID is an odd number')
        query_mask = np.logical_and(np.array(out_of_sample_origin_df['StructureID']==1),\
        np.array(out_of_sample_origin_df['AtomID']%2==1))
        print('These are the out of sample IDs')
        print(np.array(out_of_sample_origin_df[query_mask].index))
        print('The corresponding rows in the whole table:')
        print(out_of_sample_origin_df.iloc[np.array(out_of_sample_origin_df[query_mask].index)])

    Output

    ..code-block::

            StructureID  AtomID
        3             0       3
        6             0       6
        25            1       1
        30            1       6
        34            1      10
        The Structure IDs array
        [0 0 1 1 1]

        The Atom IDs array
        [ 3  6  1  6 10]

        The IDs in out_of_sample_set of StructureID==1 and AtomID is an odd number
        These are the out of sample IDs
        [25 27 29 31 33 35 37 39 41 43 45 47 49 51 53]
        The corresponding rows in the whole table:
            StructureID  AtomID
        25            1       1
        27            1       3
        29            1       5
        31            1       7
        33            1       9
        35            1      11
        37            1      13
        39            1      15
        41            1      17
        43            1      19
        45            1      21
        47            1      23
        49            1      25
        51            1      27
        53            1      29

    Here, structure ID refers the order in ``new_structures``, which is independent of the ``training_set`` nor the list ``structures`` in :ref:`Descriptor Construction <tutorial_descriptor>`.

Embedding
---------
.. toggle ::

    We then apply the embedding operation established in the DECAF model with the operation ``.embed(out_of_sample_set)``. (NOT ``get_embed()``)
    It will then return the embedded descriptors of this out-of-sample dataset.

Example (Embedding)
^^^^^^^^^^^^^^^^^^^
.. toggle ::

    .. code-block:: python
        :linenos:

        out_of_sample_embedded = embed_cluster.embed(out_of_sample_set)

    Marking the out-of-sample data with black crosses and the training set with gray dots, the embedded space looks like this

    .. image:: figures/out_of_sample_embedded_monochrome.png
        :width: 90%
        :align: center

Clustering
----------
.. toggle ::

    We then apply the clustering operation established in the DECAF model with the operation ``.classify(out_of_sample_embedded)``. (NOT ``get_cluster()``)
    It will then return the group memberships of each group ID (in the same format of ``.t_gID``). 
    The data IDs inside the returns correspond to that in the ``out_of_sample_embedded``.

    The returns depend on the setting provided in ``get_cluster(label_all= ... )`` 
    The attribute ``.label_all`` was saved during this initalization.

    .. note::
    
        If ``embed_cluster.label_all=="LabelAll"``
            .classify(out_of_sample_embedded) will return the memberships by locating the closest cluster in the model.
        
        If ``embed_cluster.label_all=="LabelDistinct"``
            .classify(out_of_sample_embedded) will return the memberships by locating the closest cluster in the model.
            Data point(s) which are separated from all clusters by at least ``.bandwidth`` will be labeled as unclassifiable and recorded in the last reserved list in ther ``return``.
        
        If ``embed_cluster.label_all=="Both"``
            .classify(out_of_sample_embedded) will both of the above, the first one is from "LabelDistinct"; the second one is "LabelAll".

    When determining the "closest" cluster, 
    there are two options in DECAF in ``get_cluster(classify_mode= ... )`` 
    The attribute ``.classify_mode`` was saved during this initalization.

    .. note::
    
        If ``embed_cluster.classify_mode=="ClusterCenter"``
            The separation of a data point from a cluster is determined with the distance from the cluster center of each cluster.
        
        If ``embed_cluster.classify_mode=="TrainingData"``
            The separation of a data point from a cluster is determined with the distance the closest data point in ``.t_embedded``

Example (Clustering)
^^^^^^^^^^^^^^^^^^^^
.. toggle ::
    
    In our examples so far, we have ``label_all=="Both"``.

    .. code-block:: python
        :linenos:

         out_of_sample_gID_distinct, out_of_sample_gID_label_all = embed_cluster.classify(out_of_sample_embedded)

    Some examples of queries:

    .. code-block:: python
        :linenos:

        print(f'The label_all settings embed_cluster.label_all is {embed_cluster.label_all}\n')
        print(f'In the return for LabelDistinct (out_of_sample_gID_distinct)')
        print(f'The last list in this membership output is reserved from unclassifiable data in out_of_sample_embedded.\nIn this case:\n{out_of_sample_gID_distinct[-1]}')
        print(f'The length of out_of_sample_gID_distinct is embed_cluster.groupnum+1: {len(out_of_sample_gID_distinct)}\n')

        print(f'In the return for LabelAll (out_of_sample_gID_label_all)')
        print(f'The last list has the same role, but always empty\nIn this case:\n{out_of_sample_gID_label_all[-1]}')
        print(f'The length of out_of_sample_gID_label_all is embed_cluster.groupnum+1: {len(out_of_sample_gID_label_all)}\n')

        print(f'Other than that, the LabelAll version and LabelDistinct are the same. Result of sanity check:')
        for groupID in np.arange(embed_cluster.groupnum):
            sanity_check = np.isin(out_of_sample_gID_distinct[groupID],out_of_sample_gID_label_all[groupID])
            if False in sanity_check:
                print(f'Some members in _distinct are not in _label_all, for group {groupID}')
                break
        else:
            print(f'Every member in each group in _distinct is in corresponding group in _label_all')

    Output

    .. code-block:: 

        The label_all settings embed_cluster.label_all is Both

        In the return for LabelDistinct (out_of_sample_gID_distinct)
        The last list in this membership output is reserved from unclassifiable data in out_of_sample_embedded.
        In this case:
        [ 2  3  6 10 16 20]
        The length of out_of_sample_gID_distinct is embed_cluster.groupnum+1: 8

        In the return for LabelAll (out_of_sample_gID_label_all)
        The last list has the same role, but always empty
        In this case:
        []
        The length of out_of_sample_gID_label_all is embed_cluster.groupnum+1: 8

        Other than that, the LabelAll version and LabelDistinct are the same. Result of sanity check:
        Every member in each group in _distinct is in corresponding group in _label_all
    
    Marking the data from ``out_of_sample_embedded`` as crosses, with the color according to the group color code in :ref:`Clustering Tutorial <tutorial_clustering>`

    .. image:: figures/out_of_sample_embedded_colored.png
        :width: 90%
        :align: center
    
    The majority of data are classified into groups done before. The data correspond to ``out_of_sample_gID_distinct[-1]`` are the black crosses, which are distinctly separated from other data.

    To further visualize the grouping in atomic structures, we make use of the book keeping above, similar to that done in :ref:`Clustering Tutorial <tutorial_clustering>`.

    .. code-block:: python
        :linenos:

        # This gives a list of groupID labels, corresponding to the order of data in .t_embedded
        # We will use it for labeling
        out_of_sample_linear_g_labels = embed_cluster.reformat_groupID(out_of_sample_gID_distinct)

        # The strcutures list was defined at the very begining
        for struct_id,struct in enumerate(new_structures):
            # Take the IDs in training corresponding to the structure
            # id_pairs[:,0] is the ID in t_embedded; id_pairs[:,1] is the atom IDs
            mask = out_of_sample_origin_df['StructureID']==struct_id
            id_pairs = np.vstack((out_of_sample_origin_df[mask].index, np.array(out_of_sample_origin_df[mask]['AtomID']))).T

            # Get the colors for atoms in this structure
            color_to_plot = out_of_sample_color_list[id_pairs[:,0]]
            # We will label the IDs on atoms, get a convenient contrast (but ugly) color for each atom
            contrast_text_color = np.tile(np.array([1,1,1,2]),(color_to_plot.shape[0],1))-color_to_plot
            
            # Adjest according to plot_atoms functionality
            struct_pos = struct.positions
            struct_pos = struct_pos - np.min(struct_pos,axis=0)

            # Adjustments to put text labels
            radii = covalent_radii[struct.numbers]
            X1 = (struct_pos - radii[:,None]).min(0)
            X2 = (struct_pos + radii[:,None]).max(0)
            M = (X1 + X2) / 2
            S = 1.05 * (X2 - X1)
            scale=0.9
            w = scale * S[0]
            maxwidth=500
            if w > maxwidth:
                w = maxwidth
                scale = w / S[0]
            h = scale * S[1]
            offset = np.array([scale * M[0] - w / 2, scale * M[1] - h / 2, 0])

            # typical matplotlib operations
            fig, ax = plt.subplots(1, 3, figsize=(30, 10))
            # This is the ASE plot atoms interface to make 2D plot atoms
            plot_atoms(struct, offset=(0,0), radii=0.9, ax=ax[0],colors=color_to_plot)
            plot_atoms(struct, offset=(0,0), radii=0.9, ax=ax[1],colors=color_to_plot)
            plot_atoms(struct, offset=(0,0), radii=0.9, ax=ax[2],colors=color_to_plot)
            # Label the IDs with text label
            for aID in id_pairs:
                ax[0].text(struct_pos[aID[1],0]-offset[0],struct_pos[aID[1],1]-offset[1],str(aID[1]),size=30,\
                        ha='center',va='center_baseline',color=contrast_text_color[aID[1]])
                ax[1].text(struct_pos[aID[1],0]-offset[0],struct_pos[aID[1],1]-offset[1],str(aID[0]),size=30,\
                        ha='center',va='center_baseline',color=contrast_text_color[aID[1]])
                ax[2].text(struct_pos[aID[1],0]-offset[0],struct_pos[aID[1],1]-offset[1],str(out_of_sample_linear_g_labels[aID[0]]),size=30,\
                        ha='center',va='center_baseline',color=contrast_text_color[aID[1]])
            ax[0].text(np.mean(struct_pos,axis=0)[0]-offset[0],np.min(struct_pos,axis=0)[1]-offset[1]-np.max(radii),\
                    f'Atom IDs in StructureID: {struct_id}\n{new_filenames[struct_id]}',size=30,ha='center',va='center_baseline')
            ax[1].text(np.mean(struct_pos,axis=0)[0]-offset[0],np.min(struct_pos,axis=0)[1]-offset[1]-np.max(radii),\
                    f'Datum IDs in out_of_sample_set',size=30,ha='center',va='center_baseline')
            ax[2].text(np.mean(struct_pos,axis=0)[0]-offset[0],np.min(struct_pos,axis=0)[1]-offset[1]-np.max(radii),\
                    f'Group ID Labels in out_of_sample_set',size=30,ha='center',va='center_baseline')
            ax[0].set_axis_off()
            ax[1].set_axis_off()
            ax[2].set_axis_off()
            fig.tight_layout()
            plt.show()
            plt.close()
    
    .. image:: figures/out_of_cluster_PAH_phenanthren_color.png
        :width: 100%
        :align: center

    .. image:: figures/out_of_cluster_PAH_tetracene_color.png
        :width: 100%
        :align: center

    As one may expect, every atom in a tetracene are classifable. 
    The difference in local atomic environments in a tetracene is beyond the cut-off radius of the descriptor, 
    it does not capture anything new beyond those in the anthracene.

    On the other hand, the twist in phenanthren presents some more local environments distinct from all in benzene, naphthalene and anthracene.
    The black atoms correspond to those black crosses on the embedded plots about, also correspond to ``out_of_sample_gID_distinct[-1]``