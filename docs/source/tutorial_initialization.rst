.. _tutorial_initialization:

Initalization of Class Object ``decaf.embed_cluster``
=====================================================

Choices of Methods
------------------

Before doing embedding and clustering of ``training_set``, we have to initalize the object class of DECAF.
In this step, we choose the types of methods we are going to use for embedding and clustering of the ``training_set``.
Below is a list of methods we already implemented, discussion about each options will be left in the corresonding tutorial section.

For embedding (arguement ``embed_str``):

    * ``cMDS``: Classical multidimensional scaling (default setting)
    * ``PCA`` : Principal components analysis 
    * ``kPCA``: Kernel principal component analysis
    * ``SketchMap``: Sketch map [1_]

For clustering (arguement ``cluster_str``):

    * ``MSC``: Mean shift clustering (default setting)
    * ``HDBSCAN``: Hierarchical density based clustering[2_]

Attributes Saved in ``decaf.embed_cluster``
-------------------------------------------

After initialization, the data (``training_set``) saved as an attribute ``.t_data``.


Example
-------

Below is a demonstration continue from the example :ref:`Descriptor Construction <tutorial_descriptor>`, with ``training_set`` is ready for input.

.. code-block:: python
    :linenos:

    embed_cluster = decaf.embed_cluster(training_set,embed_str='PCA', cluster_str='MSC')

The shape of ``.t_data``

.. code-block:: python
    :linenos:

    print(f'Shape of the input data')
    print(training_set.shape)

    print(f'Shape of the saved t_data')
    print(embed_cluster.t_data.shape)

    print(f'Sanity about the difference')
    print(np.max(np.linalg.norm(training_set - embed_cluster.t_data ,axis=1)))

Outputs

.. code-block:: 

    Shape of the input data
    (54, 824)
    Shape of the saved t_data
    (54, 824)
    Sanity about the difference
    0.0

.. rubric:: References

.. [1] M. Ceriotti, G. A. Tribello, and M. Parrinello. Simplifying the representation of complex free-energy landscapes using sketch-map. Proc. Natl. Acad. Sci. U.S.A. 108, 13023–13028 (2011). `doi:10.1073/pnas.1108486108 <https://doi.org/10.1073/pnas.1108486108>`_

.. [2] L. McInnes, J. Healy, and S. Astels. hdbscan: Hierarchical density based clustering. J. Open Source Softw. 2, 205 (2017). `doi:10.21105/joss.00205 <https://doi.org/10.21105/joss.00205>`_