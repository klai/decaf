Attributes Stored in ``decaf.embed_cluster``
============================================
If you have gone through the tutorials, you may notice that the class object ``decaf.embed_cluster`` contains most of the results and parameters. 
On this page, we give a quick summary of what is saved in this class and at which point in the procedure they are defined.


Training Data (Please refer :ref:`Initalization of Class Object <tutorial_initialization>`)
    * ``.t_data``
        The full descriptor of the training data.
        Initialize when ``decaf.embed_cluster`` is initialized.
        
Embedding Related (Please refer :ref:`Embedding Tutorial <tutorial_embedding>`)
    
    Initialize when ``.get_embed(...)`` is executed.

    * ``.dim``:
        The dimension of the embedding, will be the chosen number of dimension if estimated with broken-stick model. (NOT 0)
    * ``.t_embedded``:
        The embedded data in ``.dim`` dimensions.
        Dimensionally reduced data (embedded) for the training set ``t_data``.
        It is a ``numpy.ndarray`` in shape ``(.t_data.shape[0], .dim)``.
    * ``.embed_op``:
        An ``numpy.ndarray`` used as an operator for embedding. The shape of it and how to use it depend on which method you choose.
    * ``.evals``:
        The eigenvalues obtained in the PCA, kPCA or cMDS procedures.
        The eigenvalues in descending order, saved in descending order.
    * ``.evec`` :
        The COLUMNs are the eigenvectors obtained in the PCA, kPCA or cMDS procedures.
        The order of eigenvectors corresponds to that in ``.evals``.

Clustering Related (Please refer :ref:`Clustering Tutorial <tutorial_clustering>`)

    Initialize when ``.get_cluster(...)`` is executed.

    * ``.sigma``, ``.label_all``, ``.classify_mode``:
        Records of the settings into ``.get_cluster(...)``.
    * ``.groupnum``:
        The number of groups identified in ``.t_embedded``.
    * ``.bandwidth``:
        The bandwidth actually used to perform the clustering.  (NOT 0)
    * ``.t_gID``:
        The membership of each group ID. The ``len(class_obj.t_gID)== class_obj.groupnum+1``.
        e.g. ``.t_gID[5]`` is a ``numpy.ndarray`` of IDs in ``.t_embedded`` which are classified was group 5.
        The last list ``.t_gID[-1]`` is reserved for data being too distinct to be classified.

.. important ::
    
    * Descriptor settings are not saved in ``decaf.embed_cluster``. (Please refer :ref:`Descriptor Construction <tutorial_descriptor>`)
    * Out of sample classification data nor result is saved in ``decaf.embed_cluster``. (Please refer :ref:`Out-of-sample Classification <tutorial_out_of_sample>`)