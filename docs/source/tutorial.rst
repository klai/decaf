.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial_overview.rst
   tutorial_descriptor.rst
   tutorial_initialization.rst
   tutorial_embedding.rst
   tutorial_clustering.rst
   tutorial_out_of_sample.rst
   tutorial_class_obj.rst
   tutorial_faq.rst
