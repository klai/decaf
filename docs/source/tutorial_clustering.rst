.. _tutorial_clustering:

Clustering the Embedded Dataset
===============================

Clustering Operation and input
------------------------------

After initializing the class object ``decaf.embed_cluster(...)`` and performed the embedding of ``training_set`` with ``.get_embed(...)``, 
we proceed to clustering the embedded training set into groups.
The corresponding functions would be wrapped with the function inside the class ``.get_cluster(...)``.
The arguement various depends on the choice of clustering method you chose the class was initalized.

While the arguments in ``decaf.embed_cluster(...)`` depend on the choice of method, we will focus on classical mean shift clustering on this page.
For mean shift clustering, the key argument is ``bandwidth``. 
DECAF also provides heurestics to estimate a bandwidth based on the the similarity among data points in ``training_set``, further elaboration is given below.

    * ``bandwidth``:
        The length scale in between data points used in the algorithm. If ``bandwidth=0``, DECAF will estimate an appropriate bandwdith.
    * ``classify_mode``:
        For out-of-sample classification. Compare the data point(s) to ``"ClusterCenter"`` or all ``"TrainingData"``. 
        The default is ``"ClusterCenter"``. Please refer to :ref:`Out-of-sample Classificaiton <tutorial_out_of_sample>` for details.
    * ``label_all``:
        For out-of-sample classification. If the data is very far away (larger than ``.bandwidth``) from all clusters in the model, 
        DECAF labels the data un-classifiable (option ``"LabelDistinct"``) or it labels the data to the closest cluster (option ``"LabelAll"``).
        Alternatively, it returns results from both approaches seperately (option ``"Both"``). The default is ``"LabelAll"``. 
        Please refer to :ref:`Out-of-sample Classificaiton <tutorial_out_of_sample>` for details.

Other settings will become relevant when turning on the bandwidth estimation option.

Estimation of ``bandwidth``:
----------------------------
.. toggle::

    Unlike the popular k-means clustering algorithm, mean shift clustering does not require a preassumed number of groups.
    On the other hand, the choice of bandwidth is the key to the resolution of mean shift clustering. 
    An extremely oversized bandwidth will group all data points into one group; an essentially zero bandwidth will separate each data point into its own group. 
    There is no right or wrong choice of a classfication, an optimal choice depends on the use cases and statistics on the dataset.

    To automatize the wokrflow, DECAF estimates the optimal bandwidth by constructing a density function histogram of the pairwise distances among each pair of data points in ``.t_embedded``. 
    Consider a dataset with data points embedded in a 2D space, with a Gaussian distribution. Such histogram of the pairwise-distances will be like this:

    .. image:: figures/1_blob.png
        :width: 70%
        :align: center
    .. image:: figures/1_blob_distribution.png
        :width: 90%
        :align: center

    Now with two separate blobs of the data points, such histogram will naturally be like the following.
    The first peak corresponds to the pairwise distances of data within the same blob.; the second peak corresponds to the pairwise distances between data points from two different blobs.

    .. image:: figures/2_blob.png
        :width: 70%
        :align: center
    .. image:: figures/2_blob_distribution.png
        :width: 90%
        :align: center

    .. note::

        A precise description of the y-axis of the histogram is: the estimated probability density function of a pairwise distance at a certain value.
        The area under the curve of this histogram is thus normalized to one.

    Based on this picture, DECAF constructs a density histogram of the pairwise distances within ``.t_embedded``. 
    It then takes the first minima as the estimated bandwidth. However, the question now becomes "what makes an appropriate histogram (e.g. what is a good binwidth)?". 
    DECAF gives two options based on situations, ``Gaussian`` kernel density estimation or ``HaarWavelet`` expansion 

    For ``bandwidth_estimate="Gaussian"``:
    
    .. note::

        For this option, we apply Gaussian kernel density estimation on the pairwise distances. 
        The followings are the arguements into ``.get_cluster()`` related to this method. 

        * ``sigma``:
            The width of the Gaussian used in KDE, the value should be adjusted based on the norm of the input ``training_set``. 
            A smaller value of ``sigma`` will give a finer resolution of the histogram, thus in general a smaller ``bandwidth``.

    For ``bandwidth_estimate="HaarWavelet"``:
    
    .. note::

        While producing a smooth and detailed histogram to guide the bandwidth estimation, Gaussian KDE can be computationally heavy in workload.
        An alternative estimation approach is to expand the density of pairwise distances with Haar wavelet functions, this gives a less detailed but faster estimation.
        This option is more appropriate when the number of data points is at least 500 in ``training_set``.

        The followings are the arguements into ``.get_cluster()`` related to this method. 

        * ``sigma``:
            The tolerance to truncation error when proceeding to a higher degree of Haar wavelet expansion.
            Value ranges from 0 to 1.0. In contrast to ``bandwidth_estimate="Gaussian"`` setting,
            the higher theis value will give a high more detailed histogram, thus in general a smaller ``bandwidth``.
        * ``max_layer``:
            The maximum degree in expansion, if set to zero, there is no maximum number of degree in expansion.
        * ``min_layer``:
            The minimum degree in expansion, if set to zero, there is no minimum number of degree in expansion.
        * ``consecutive_fail``:
            If the expansion of a high degree expansion increases the error beyond the ``sigma`` tolerance for ``consecutive_fail`` times, the expansion will be terminated.
            The default value is 1, setting this option to a higher value will brult force the estimation to a higher resolution in the histogram, thus a smaller ``bandiwdth``.

Attributes Saved in ``decaf.embed_cluster``
-------------------------------------------

After ``.get_cluster(...)`` results and attributes will be saved as the following list, instead of through ``return``.

    * ``.sigma``, ``.label_all``, ``.classify_mode``:
        Records of the settings, correspond to the inputs discussed above.
    * ``.groupnum``:
        The number of groups identified in ``.t_embedded``.
    * ``.bandwidth``:
        The bandwidth actually used to perform the clustering.  (NOT 0)
    * ``.t_gID``:
        The membership of each group ID. The ``len(class_obj.t_gID)== class_obj.groupnum+1``.
        e.g. ``.t_gID[5]`` is a ``numpy.ndarray`` of IDs in ``.t_embedded`` which are classified was group 5.
        The last list ``.t_gID[-1]`` is reserved for data being too distinct to be classified.

If bandwidth estimation is performed (``.get_cluster(bandwidth=0, ...)``), further attributes are saved:

    * ``.hist_distribution``, ``.hist_bins``, ``.hist_binwidth``:
        The density function, locations of bins, corresponding binwidths shown in the histogram above.
    * ``.bandwidth_full``:
        A full set of minima in the density function histogram, this serves as a candidate list for clustering. 
        Please refer :ref:`Bandwidth Finetunning <tutorial_more_finetune>` for elaboration.

Example
-------

Following the example in :ref:`Embedding <tutorial_embedding>`, we will perform mean shift clustering on the PCA embedded data ``.t_embedded``.

.. code-block:: python
    :linenos:

    # Setting the clustering method (also embedding method) during initaliziation
    embed_cluster = decaf.embed_cluster(training_set,embed_str='PCA',cluster_str='MSC')
    # Setting the dimension estimatin on by dim=0 (same as previous example)
    embed_cluster.get_embed(dim=0)
    # Clustering, with the bandwidth estimation using Gaussian KDE
    embed_cluster.get_cluster(bandwidth=0, bandwidth_estimate='Gaussian', label_all='Both', classify_mode='TrainingData')

Basic queries:

.. code-block:: python
    :linenos:

    print(f'Number of groups {embed_cluster.groupnum}')
    print(f'The length of .t_gID: is 1+number of groups ({len(embed_cluster.t_gID)})')
    print(f'The membership of group, e.g. group ID=4:\n{embed_cluster.t_gID[4]}')
    print(f'Choice of bandwidth {embed_cluster.bandwidth}')
    print(f'Full list of minima of density of pairwise distance\n{embed_cluster.bandwidth_full}')

Output

.. code-block::

    Number of groups 7
    The length of .t_gID: is 1+number of groups (8)
    The membership of group, e.g. group ID=4:
    [48 49]
    Choice of bandwidth 0.023005590417762196
    Full list of minima of density of pairwise distance
    [0.02300559 0.06463485 0.1365001  0.31002839 0.48092746 0.61326469
    1.01290559 1.77055814]

.. tip:: 

    For convenience you may need a list of group ID labels of each data point in the order corresponds to the ``.t_embedded``.
    In that case, you may call the method ``embed_cluster.reformat_groupID( embed_cluster.t_gID )``
    Following the above example:
    
    .. code-block:: python
        :linenos:

        linear_g_labels = embed_cluster.reformat_groupID(embed_cluster.t_gID)
        print(f'The group ID labels of the first 12 data points in .t_embedded:\n{linear_g_labels[:12]}')

    Output

    .. code-block::

        The group ID labels of the first 12 data points in .t_embedded:
        [0 0 0 0 0 0 6 6 6 6 6 6]

    The first 6 data points are labeled as group ID 0; the next 6 are labeled as group ID 6. 
    This result is reasonable as we already knew the first 12 data points are from the benzene ring.
    The first 6 atoms and the next 6 atoms are carbon and hydrogen atoms, which are posited at two completely different local atomic environments.

As a reference, below is the density function histogram for bandwidth estimation. The first vertical line corresponds to the current estimation from DECAF.

.. image:: figures/PCA_Bandwidth.png
    :width: 90%
    :align: center

The embedded data with colors of the groups

.. code-block:: python
    :linenos:

    import matplotlib.pyplot as plt
    from matplotlib import cm
    from matplotlib.colors import Normalize

    # An example of making a color code for each group ID
    norm = Normalize(vmin=-0.5, vmax=embed_cluster.groupnum-0.5)
    mapclass=cm.ScalarMappable(norm=norm, cmap='jet')
    color_code={gid: mapclass.to_rgba(gid) for gid in np.arange(embed_cluster.groupnum)}

    # An example of assigning colors to each data in .t_embedded, based on group
    # This gives a list of groupID labels, corresponding to the order of data in .t_embedded
    linear_g_labels = embed_cluster.reformat_groupID(embed_cluster.t_gID)
    color_list = np.array([color_code[data_id] for data_id in linear_g_labels])

    # Similar to the previous plotting example in embedding tutorial, but this one uses the defined code code above
    fig, ax = plt.subplots(1,2,figsize=(3.54*2,3.54))
    fig.patch.set_facecolor('white')
    ax[0].set_xlabel("Embedded Dimension 1", fontsize=16)
    ax[0].set_ylabel(r"Embedded Dimension 2", fontsize=16)
    ax[0].scatter(embed_cluster.t_embedded[:,0],embed_cluster.t_embedded[:,1], color=color_list)
    ax[1].set_xlabel("Embedded Dimension 1", fontsize=16)
    ax[1].set_ylabel(r"Embedded Dimension 3", fontsize=16)
    ax[1].scatter(embed_cluster.t_embedded[:,0],embed_cluster.t_embedded[:,2], color=color_list)
    fig.tight_layout()
    plt.show()
    plt.close()

.. image:: figures/embedded_colored.png
    :width: 90%
    :align: center

It is clear that the three components have different roles in seperating the data in this embedded space. 
The first dimension seperate the two space into two halves; the second dimension is sensitive to the differences among the data with negative 1st component; 
the third dimension is sensitive instead to the differences among the data with positive 1st component.

.. important::
    DECAF only sees numerical data of the descriptors (in ``training_set``), whether this grouping is optimal or suitable to your use case is always subjective. 
    To do such judgement, user should query the results in a (active) learning workflow (for bigger database); or compare to users' insight (for smaller datasets). 

    In either case, we have to track back the origins of each data point in ``training_set``, which atoms in which structures they correspond to. 
    Please see the following demonstration.

Now, to actually visualize the atomic structures with grouping, we need the constructred :ref:`Book Keeping <tutorial_bookkeeping>` constructed like before.
The most trivial way is to paint the atoms with colors according to the color code. ``color_list`` in the following script is already defined above.

.. code-block:: python
    :linenos:

    from ase.visualize.plot import plot_atoms
    import matplotlib.pyplot as plt
    from ase.data import atomic_numbers, covalent_radii

    # This gives a list of groupID labels, corresponding to the order of data in .t_embedded
    # We will use it for labeling
    linear_g_labels = embed_cluster.reformat_groupID(embed_cluster.t_gID)

    # The strcutures list was defined at the very begining
    for struct_id,struct in enumerate(structures):
        # Take the IDs in training corresponding to the structure
        # id_pairs[:,0] is the ID in t_embedded; id_pairs[:,1] is the atom IDs
        mask = training_set_origin_df['StructureID']==struct_id
        id_pairs = np.vstack((training_set_origin_df[mask].index, np.array(training_set_origin_df[mask]['AtomID']))).T

        # Get the colors for atoms in this structure
        color_to_plot = color_list[id_pairs[:,0]]
        # We will label the IDs on atoms, get a convenient contrast (but ugly) color for each atom
        contrast_text_color = np.tile(np.array([1,1,1,2]),(color_to_plot.shape[0],1))-color_to_plot
        
        # Adjest according to plot_atoms functionality
        struct_pos = struct.positions
        struct_pos = struct_pos - np.min(struct_pos,axis=0)

        # Adjustments to put text labels
        radii = covalent_radii[struct.numbers]
        X1 = (struct_pos - radii[:,None]).min(0)
        X2 = (struct_pos + radii[:,None]).max(0)
        M = (X1 + X2) / 2
        S = 1.05 * (X2 - X1)
        scale=0.9
        w = scale * S[0]
        maxwidth=500
        if w > maxwidth:
            w = maxwidth
            scale = w / S[0]
        h = scale * S[1]
        offset = np.array([scale * M[0] - w / 2, scale * M[1] - h / 2, 0])

        # typical matplotlib operations
        fig, ax = plt.subplots(1, 3, figsize=(30, 10))
        # This is the ASE plot atoms interface to make 2D plot atoms
        plot_atoms(struct, offset=(0,0), radii=0.9, ax=ax[0],colors=color_to_plot)
        plot_atoms(struct, offset=(0,0), radii=0.9, ax=ax[1],colors=color_to_plot)
        plot_atoms(struct, offset=(0,0), radii=0.9, ax=ax[2],colors=color_to_plot)
        # Label the IDs with text label
        for aID in id_pairs:
            ax[0].text(struct_pos[aID[1],0]-offset[0],struct_pos[aID[1],1]-offset[1],str(aID[1]),size=30,\
                    ha='center',va='center_baseline',color=contrast_text_color[aID[1]])
            ax[1].text(struct_pos[aID[1],0]-offset[0],struct_pos[aID[1],1]-offset[1],str(aID[0]),size=30,\
                    ha='center',va='center_baseline',color=contrast_text_color[aID[1]])
            ax[2].text(struct_pos[aID[1],0]-offset[0],struct_pos[aID[1],1]-offset[1],str(linear_g_labels[aID[0]]),size=30,\
                    ha='center',va='center_baseline',color=contrast_text_color[aID[1]])
        ax[0].text(np.mean(struct_pos,axis=0)[0]-offset[0],np.min(struct_pos,axis=0)[1]-offset[1]-np.max(radii),\
                f'Atom IDs in StructureID: {struct_id}\n{filenames[struct_id]}',size=30,ha='center',va='center_baseline')
        ax[1].text(np.mean(struct_pos,axis=0)[0]-offset[0],np.min(struct_pos,axis=0)[1]-offset[1]-np.max(radii),\
                f'Datum IDs in training_set',size=30,ha='center',va='center_baseline')
        ax[2].text(np.mean(struct_pos,axis=0)[0]-offset[0],np.min(struct_pos,axis=0)[1]-offset[1]-np.max(radii),\
                f'Group ID Labels in training_set',size=30,ha='center',va='center_baseline')
        ax[0].set_axis_off()
        ax[1].set_axis_off()
        ax[2].set_axis_off()
        fig.tight_layout()
        plt.show()
        plt.close()

.. image:: figures/PAH_benzene_color.png
    :width: 100%
    :align: center

.. image:: figures/PAH_naphthalene_color.png
    :width: 100%
    :align: center

.. image:: figures/PAH_anthracene_color.png
    :width: 100%
    :align: center

Now we can see, from the atomic structures, the carbon atoms (groups 0, 1, 2, 3) correspond to the data with smaller first component in the embedded space. 
The grouping is consistent to the symmetry, judged by human insight. However, whether this grouping resolution is enough or not is once again subjective. 
Further demonstration with a different settings is shown below.

.. _tutorial_more_finetune:

Bandwidth Finetunning
---------------------
.. toggle::

    If the resolution of the grouping is not appropriate (for examples, too detailed), one can use the attribute ``.bandwidth_full`` as a reference to finetune the clustering.
    In this demonstration, let's say we want to reduce the number of groups.

    .. code-block:: python
        :linenos:

        # Operations (e.g. .get_cluster()) will overwrite the grouping in embed_cluster class,
        # We will however continue the demonstration on other pages with the model above,
        # For convenience, we demonstrate here with a copy of the model
        from copy import deepcopy
        copy_embed_cluster = deepcopy(embed_cluster)

        # Taking the fourth minima in the full list.
        # NOTE: This .bandwidth_full is defined during the previous bandwidth estimation attempt inside .get_cluster(bandiwdth=0), which performed the computation of the full list
        # Please refer the demonstration below this to compute this list
        copy_embed_cluster.get_cluster(bandwidth=copy_embed_cluster.bandwidth_full[3], bandwidth_estimate='Gaussian')
        print(f'Number of groups {copy_embed_cluster.groupnum}')
        print(f'The length of .t_gID: is 1+number of groups ({len(copy_embed_cluster.t_gID)})')
        print(f'The membership of group, e.g. group ID=2:\n{copy_embed_cluster.t_gID[2]}')
        print(f'Choice of bandwidth {copy_embed_cluster.bandwidth}')
        print(f'Full list of minima of density of pairwise distance\n{copy_embed_cluster.bandwidth_full}')

    Output

    ..code-block::

        Number of groups 3
        The length of .t_gID: is 1+number of groups (4)
        The membership of group, e.g. group ID=2:
        [ 6  7  8  9 10 11 22 23 24 25 26 27 28 29 44 45 46 47 48 49 50 51 52 53]
        Choice of bandwidth 0.31002838913234004
        Full list of minima of density of pairwise distance
        [0.02300559 0.06463485 0.1365001  0.31002839 0.48092746 0.61326469
        1.01290559 1.77055814]

    If we generate the plots again (of course redefined the color code for new groups), then the atomic structures will look like this

    .. image:: figures/embedded_colored_less.png
        :width: 90%
        :align: center

    .. image:: figures/PAH_benzene_color_less.png
        :width: 100%
        :align: center

    .. image:: figures/PAH_naphthalene_color_less.png
        :width: 100%
        :align: center

    .. image:: figures/PAH_anthracene_color_less.png
        :width: 100%
        :align: center

    As the number of groups reduced to three, we can see all hydrogen atoms are labeled as the same type of local environments, and only two groups of carbon atoms are remaining.