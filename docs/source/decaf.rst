Modules
============

DECAF Object Class
------------------------
.. toggle:: DECAF Object Class

   The main object class that conatins most of the functionalities and methods of DECAF, except a wrapper of double-SOAP computation.

   .. autoclass:: decaf.embed_cluster
      :no-members:

Descriptor Related
------------------------
.. toggle:: Descriptor Related
   
   This is a wrapper to get double-SOAP. It is **NOT** a method in embed_cluster object.

   .. autoclass:: decaf.get_SOAP

.. currentmodule:: decaf

Embedding Related
------------------------
.. toggle:: Embedding Related

   These methods are used to get and set the attributes related to embedding.

   .. automethod:: embed_cluster.get_cMDS
   .. automethod:: embed_cluster.truncate_cMDS
   .. automethod:: embed_cluster.embed_cMDS
   .. automethod:: embed_cluster.get_PCA
   .. automethod:: embed_cluster.truncate_PCA
   .. automethod:: embed_cluster.embed_PCA
   .. automethod:: embed_cluster.get_kPCA
   .. automethod:: embed_cluster.kernel_dot
   .. automethod:: embed_cluster.truncate_kPCA
   .. automethod:: embed_cluster.embed_kPCA
   .. automethod:: embed_cluster.get_broken_stick
   .. automethod:: embed_cluster.get_SketchMap
   .. automethod:: embed_cluster.embed_SketchMap

Clustering Related
------------------------
.. toggle:: Clustering Related

   These methods are used to get and set the attributes related to clustering.

   .. automethod:: embed_cluster.get_MeanShift
   .. automethod:: embed_cluster.get_bandwidth_Gaussian
   .. automethod:: embed_cluster.get_bandwidth_HaarWavelet
   .. automethod:: embed_cluster.MSC_classify
   .. automethod:: embed_cluster.get_HDBSCAN
   .. automethod:: embed_cluster.HDBSCAN_classify

Utility Methods
------------------------
.. toggle:: Utility Methods

   Methods that provide utility functionality.

   .. automethod:: embed_cluster.workflow
   .. automethod:: embed_cluster.reformat_groupID
   .. automethod:: embed_cluster.reformat_groupID_pocket
   .. automethod:: embed_cluster.reorder_groupID
   .. automethod:: embed_cluster.manual_group
   .. automethod:: embed_cluster.get_db_plottings
   .. automethod:: embed_cluster.get_embed_plottings
