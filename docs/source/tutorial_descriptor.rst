.. _tutorial_descriptor:

Descriptor Construction
=======================

Formating
----------

DECAF is modular and users can choose what vectorial descriptor they want to use to describe local atomic environments. However, here are some basic requirements to proceed.
For convenience, we will call the input for the next step ``training_set``, the format has to satisfy the followings

.. important::

    * ``training_set`` is a ``numpy.ndarray``
    * ``training_set`` has a shape ``(N,M)``, ``N`` is the number of data points in the training set; ``M`` is the number of features in the descriptor.

Typical choices would be SOAP[1_] descirptor or MACE[2_] descriptor.
For simple SOAP descriptor, user may obtain them with `DScribe <https://singroup.github.io/dscribe/latest/tutorials/descriptors/soap.html>`_; for MACE descriptor, user may obtain it with `mace <https://mace-docs.readthedocs.io/en/latest/guide/descriptors.html>`_. Please refer to the corresponding official documentation for details.

In DECAF, we provide a wrapper to construct a so-called double-SOAP.    
A double-SOAP is a vectorial descriptor concatenated with two SOAP descriptors, each of different settings. Each of these descriptor would typically have different cut-off radii, thus it brings higher resolution in short range, compared to a single SOAP descriptor with a large cut-off radius. 

.. tip:: 

    The descriptor of each atom is describing the configuration around (where atoms are positioned around this atom).
    The descriptor is so-called a local descriptor, because it does not capture the configuration of infinite range.
    Instead, it only keep the information within a cut-off radius.
    The optimal choice of cut-off radii thus depends on the range of interatomic interaction in the system.

Example
-------

Example of constructing a training set ``training_set`` to proceed:

Let's say we are having a few molecules, benzene, naphthalene, anthracene. 
You can find these files inside the `repository <https://gitlab.mpcdf.mpg.de/klai/decaf/-/tree/master/examples/Structures?ref_type=heads>`_ . 
We want to put them into groups of local environments. Below we start with computing the SOAP descriptors of local atomic environments in them, 
and we will carry this example to other demonstrations to other tutorials.

.. code-block:: python
    :linenos:

    import numpy as np
    from ase import io as aseio
    import decaf

    # Typical procedures to load in the ASE Atoms from files, here we are using .con,
    # please adapt accordingly in your situations.
    filenames=['PAH_benzene.con','PAH_naphthalene.con','PAH_anthracene.con']
    # This line has to be edited if you have multiple structures in one file
    structures=[aseio.read(filename) for filename in filenames]

    # Length-scale in angstrom, this a normalization factor of cut-off radii, sigmas below,
    # a typcial nearest neighbor separartion in the system would be appropriate
    # For carbon-carbon bonding, we take ~1.42\AA
    lengthscale = 1.42

    # Settings for the short range SOAP:
    # Cut-off of short range SOAP, in the middle of 1st and 2nd coordination shell of carbon in graphene
    # decaf.get_SOAP() will pass (SOAP_S_cut * lengthscale) into DScribe
    SOAP_S_cut=(1+np.sqrt(3))/2
    # Usually 1/8 of the cut-off would be appropriate, but please tune it around to optimize
    # decaf.get_SOAP() will pass (SOAP_S_sigma * lengthscale) into DScribe
    SOAP_S_sigma=SOAP_S_cut/8
    # The maximum degrees of expansion of the spherical harmonics in SOAP computation
    SOAP_S_nmax=8
    SOAP_S_lmax=4

    # Settings for the long range SOAP:
    # Cut-off of long range SOAP, in the middle of 3rd and 4th coordination shell of carbon in graphene
    # decaf.get_SOAP() will pass (SOAP_L_rcut * lengthscale) into DScribe
    SOAP_L_cut=(2+3)/2
    # Usually 1/8 of the cut-off would be appropriate, but please tune it around to optimize
    # decaf.get_SOAP() will pass (SOAP_L_sigma * lengthscale) into DScribe
    SOAP_L_sigma=SOAP_L_cut/8
    # The maximum degrees of expansion of the spherical harmonics in SOAP computation
    SOAP_L_nmax=4
    SOAP_L_lmax=3

    # The list of chemical species (atomic number) included in the computation of SOAP
    # In this example, we take all chemical species in the system [1,6]
    species=np.unique(np.concatenate([struct.numbers for struct in structures])).astype('int')

    # Obtain the full SOAP of the structures
    # Also speicify if the structures are periodic (False in this case)
    training_set=np.concatenate([decaf.get_SOAP(struct,lengthscale,species=species,\
                                        SOAP_S_cut=SOAP_S_cut,SOAP_S_sigma=SOAP_S_sigma,\
                                        SOAP_S_nmax=SOAP_S_nmax, SOAP_S_lmax=SOAP_S_lmax,\
                                        SOAP_L_cut=SOAP_L_cut,SOAP_L_sigma=SOAP_L_sigma,\
                                        SOAP_L_nmax=SOAP_L_nmax, SOAP_L_lmax=SOAP_L_lmax,\
                                        periodic=False) for struct in structures])

We applied ``decaf.get_SOAP(...)`` on each structure separately. Each return of the ``decaf.get_SOAP(...)`` is of shape ``(n_i,M)``

.. important::

    Below is the list of arguements into ``decaf.get_SOAP(...)``

    ``struct``
        The ASE Atoms object

    ``lengthscale``
        A typcial nearest neighbor separartion in the system would be appropriate. 
        For carbon-carbon bonding, we take ~1.42 angstroms in this demonstration
    
    ``species``
        The list of chemical species (atomic number) included in the computation of SOAP
        In this example, we take all chemical species in the system [1,6]

    ``SOAP_S_cut``, ``SOAP_S_sigma``, ``SOAP_S_nmax``, ``SOAP_S_lmax``
        The settings for short-range SOAP: cut-off radius (normalized by ``lengthscale``), Gaussian sigma (normalized by ``lengthscale``), maximum degree in spherical harmonics expansions
    
    ``SOAP_L_cut``, ``SOAP_L_sigma``, ``SOAP_L_nmax``, ``SOAP_L_lmax``
        Corresponding settings for the long-range SOAP
    
    ``periodic``
        Boolean whether this structure is periodic or not

.. important::

    *  ``n_i`` is the number of atoms in structure ``i``.
    *  ``M`` is the total number of features with the SOAP settings, which is sum of the number of features of long- and short-range SOAPs.

Concatenating each returned set of SOAPs corresponding to each structure, the ``training_set`` is thus in shape ``(N,M)`` as expected.

.. _tutorial_bookkeeping:

Important: Book Keeping
-----------------------

.. important::

    ``training_set`` is all DECAF will see as a dataset, it does not keep track of which data points come from which structure(s).
    The first axis of ``training_set``, ``N`` corresponds to the ID of each data point. e.g. Datum ID=20 corresponds to ``training_set[20]``.
    On one hand, it allows full flexiblility of including only a selected set of atoms from each structures into the training set.
    On the other hand, that implies it is the user's responsibility to book keep the relation between data in DECAF and that in structure set.
    Here is an illustration how these IDs are related in the example above.

.. image:: figures/PAH_benzene.png
    :width: 90%
    :align: center
.. image:: figures/PAH_naphthalene.png
    :width: 90%
    :align: center
.. image:: figures/PAH_anthracene.png
    :width: 90%
    :align: center

.. tip::

    When approaching a large set of structures, which not all atoms in each structure are included in the ``training_set``, we advice user to keeptrack of the IDs clearly.
    Below is just one demonstration how we do it, but due to the vast flexibility how structures files and selection of ``training_set`` members, we cannot provide a module for general cases.

.. code-block:: python
    :linenos:

    # Continue the script from above
    import pandas as pd
    # The origin of each training_set datum, the first column is the structure ID, enumerated according to the input order in structures,
    # the second column is the atom ID
    training_set_origin = np.vstack([np.vstack((np.full((len(struct)),structid),np.arange(len(struct)))).T for structid,struct in enumerate(structures)])
    training_set_origin_df = pd.DataFrame({'StructureID': training_set_origin[:, 0], 'AtomID': training_set_origin[:, 1]})

Some examples of queries:

.. code-block:: python
    :linenos:

    # To query the origins of data of IDs e.g. [7.9.20,31], in the training_set
    print(training_set_origin_df.iloc[[7,9,20,31]])

    # Get the structures of these IDs
    print(f'The Structure IDs array')
    print(np.array(training_set_origin_df.iloc[[7,9,20,31]]['StructureID']))

    # Get the structures of these IDs
    print(f'\nThe Atom IDs array')
    print(np.array(training_set_origin_df.iloc[[7,9,20,31]]['AtomID']))

    # Get the training datum IDs in training_set from conditions
    print(f'\nThe IDs in training_set of StructureID==2 and AtomID is an odd number')
    query_mask = np.logical_and(np.array(training_set_origin_df['StructureID']==2),\
    np.array(training_set_origin_df['AtomID']%2==1))
    print('These are the training IDs')
    print(np.array(training_set_origin_df[query_mask].index))
    print('The corresponding rows in the whole table:')
    print(training_set_origin_df.iloc[np.array(training_set_origin_df[query_mask].index)])

Outputs

.. code-block:: 

        StructureID  AtomID
    7             0       7
    9             0       9
    20            1       8
    31            2       1
    The Structure IDs array
    [0 0 1 2]

    The Atom IDs array
    [7 9 8 1]

    The IDs in training_set of StructureID==2 and AtomID is an odd number
    These are the training IDs
    [31 33 35 37 39 41 43 45 47 49 51 53]
    The corresponding rows in the whole table:
        StructureID  AtomID
    31            2       1
    33            2       3
    35            2       5
    37            2       7
    39            2       9
    41            2      11
    43            2      13
    45            2      15
    47            2      17
    49            2      19
    51            2      21
    53            2      23

.. rubric:: References

.. [1] Albert P. Bartók, Risi Kondor, and Gábor Csányi. On representing chemical environments. Phys. Rev. B 87, 184115 (2013). `doi:10.1103/PhysRevB.87.184115 <https://doi.org/10.1103/PhysRevB.87.184115>`_

.. [2] Ilyes Batatia, Dávid Péter Kovács, Gregor N. C. Simm, Christoph Ortner, Gábor Csányi. MACE: Higher Order Equivariant Message Passing Neural Networks for Fast and Accurate Force Fields. Advances in Neural Information Processing Systems 35, 11423 (2022). `doi:10.48550/arXiv.2206.07697 <https://doi.org/10.48550/arXiv.2206.07697>`_