Overview
========

On this page, we give an overview of how to use the DECAF package, also illustrate with examples of typical tasks and use cases.

Most of the funcitons given by DECAF is wrapped in the python class ``decaf.embed_cluster``. Besides this object, DECAF also provides a wrapper to generate double-SOAP, utilizing DScribe.
A typical workflow from getting the descriptors from a dataset to obtaining group labels of each datum in the dataset is listed below:

    #. :ref:`Descriptor construction of local atomic environments from a set of atomic strucutures <tutorial_descriptor>`
    #. :ref:`Initializing the main class object <tutorial_initialization>` ``decaf.embed_cluster``
    #. :ref:`Embedding the training dataset into a lower dimensional space <tutorial_embedding>`
    #. :ref:`Clustering the embedded data points into equivalnce groups <tutorial_clustering>`

Beyond classification applied on a training set, user can apply the classification model on out-of-sample data
    * :ref:`Performing out-of-sample classification <tutorial_out_of_sample>`
